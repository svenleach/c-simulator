/*
 * ==========================================================================================================
 * Name        : HardConditions.c
 * Author      : Maria Laura Ferster
 * Version     : 1.0
 * Date		   : 08-January-2018
 * Copyright   : (c) 2018.  Maria Laura Ferster, Mobile Health Systems Lab, ETH Zurich. All rights reserved
 *
 * Description : Conditions that needs to be satisfied in order to stimulate.
 *
 * Objective   : Avoid false stimulations
 ============================================================================================================
 */

#include "HardConditions.h"

bool HC_0_SleepOnsetDelayCondition(uint32_t counter, bool INIT_SLEEP_ONSET_FLAG){

	if (INIT_SLEEP_ONSET_FLAG==0){
		INIT_SLEEP_ONSET_FLAG = counter >= INITIAL_SLEEP_ONSET_TIME_SAMP;
	}

	return INIT_SLEEP_ONSET_FLAG;
}


bool HC_1_RemSleepOnsetDelayCondition(uint32_t counter, bool INIT_F){

	bool HC1_SD_ONSET_FLAG = 0;

	if(INIT_F==1){
		HC1_SD_ONSET_FLAG = counter >= SLEEP_ONSET_TIME_SAMP;
	}

	return HC1_SD_ONSET_FLAG;
}



int HC_2_ConsecutiveTones(int STIM_CONDITION_HC2, int SWD_FLAG, int INIT_flag, int Restart, int actual_tone_time){

	bool HC2_FLAG = 0;

	static int first_tone_detected = 0;
	static int tone_counter = 0;
	static int time_between_tones = 0;
	static int prev_tone_time = 0;


	if (Restart==1){
		tone_counter = 0;
		prev_tone_time = 0;
		first_tone_detected = 0;
	}

	if(INIT_flag==1){
	//*** Basic condition: one tome per SW
	if (STIM_CONDITION_HC2==1){

		if (first_tone_detected==0){  // Detection of first tone
			if (SWD_FLAG == 1){ // redundante?
			// First tone have been detected
				first_tone_detected = 1;
				prev_tone_time = actual_tone_time;
				tone_counter++;
			}

		}else{
			// After first tone have been detected
			// Calculate the time between tones
			time_between_tones = actual_tone_time - prev_tone_time;

			if (time_between_tones < MIN_TIME_BETW_TONE_SAMP){  // consequtive tones on the same SW, avoid stimulation => STIM_CONDITION_HC2 = 0;
				HC2_FLAG = 1;
			}else{
				tone_counter++;
			}
		}

		prev_tone_time = actual_tone_time;
	}

	//*** Stimulate according to STIM_CONDITION

	switch(STIM_COND){

	case 0:
		// Play tones to every slow waves.
		break;
	case 1:
		HC2_FLAG = stim_cond_onoff_window(STIM_CONDITION_HC2, Restart, HC2_FLAG);
		break;
	}


	}

	if (SAVE_HARD_COND_2 == 1)
		fprintf(f_HARD_COND2, "%d%s ", HC2_FLAG, ",");


	return HC2_FLAG;
}


bool HC_3_MovementDetection_FromRawData(float EEG_raw){

	static int BreakCounter = 0;
	float EEG_Filt;
	bool HC3_FLAG;

	// high pass filter
	EEG_Filt = HighPassFilterT_01Hz_HC3(EEG_raw);

	if (( EEG_Filt<SW_DOWN_THRESHOLD ) || ( EEG_Filt>SW_UP_THRESHOLD )){
		BreakCounter = MOV_DET_BREAK_SAMP;
		HC3_FLAG = 1;
	}else{

		if (BreakCounter==0){
			HC3_FLAG = 0;
		}else{
			BreakCounter--;
			HC3_FLAG = 1;
		}
	}


	if (SAVE_HARD_COND_3 == 1)
		fprintf(f_HARD_COND3, "%d%s ", HC3_FLAG, ",");

	return !HC3_FLAG;
}


bool HC4_BetaEvaluation(bool HC4_FLAG, float BetaAvgMean, float BETA_THR){

	static float B_onset_time_count = 0;

	if (HC4_FLAG==0){
		if (BetaAvgMean<=BETA_THR){
			HC4_FLAG = 0;
		}else{
			B_onset_time_count = 0;
			HC4_FLAG = 1;
		}
	}else{
		B_onset_time_count++;
		if (B_onset_time_count==HC4_ONSET_TIME)
			HC4_FLAG = 0;
		else
			HC4_FLAG = 1;
	}

	return HC4_FLAG;
}

bool HC5_AlphaEvaluation(bool HC5_FLAG, float BetaAvgMean, float betaPower, float AlphaAvgMean, float alphaPower){

	static float A_onset_time_count = 0;
	float var = AlphaAvgMean/3.0 + BetaAvgMean;
	static int alphaTH_aux = 0;
	static float THR;

	if(alphaTH_aux == 0){
		THR = (alphaPower/3.0 + betaPower)*alphaTH_PG;
		fprintf(f_HC5_AVG_TH, "%f%s%f", alphaPower,",",THR);
		printf("%s\n","Hard condition 5 enabled. Threshold calculated:");
		printf("%s%f\n", "  * 10min Alpha Average power: ", alphaPower);
		printf("%s%f\n\n", "  * Threshold (AlphaPow/3 + Beta): ", THR);
		alphaTH_aux = 1;
	}

	if (HC5_FLAG==0){
		if (var<=THR){ // HERE condition
			HC5_FLAG = 0;
		}else{
			A_onset_time_count = 0;
			HC5_FLAG = 1;
		}
	}else{
		A_onset_time_count++;
		if (A_onset_time_count==HC5_ONSET_TIME)
			HC5_FLAG = 0;
		else
			HC5_FLAG = 1;
	}

	return HC5_FLAG;
}

bool HC_AraulsaDetection(float inEEGdata, int SleepOnsetFlag, int ArausalDetCond){


	// HC5 intialization variables
	static bool HC5_FLAG 		= 0;
	static float AlphaAvgMean 	= 0;
	static float alphaPower 	= 0;

	if(ENABLE_HARD_COND_5==1){

		if(SleepOnsetFlag==0)
			alphaPower = HC_5_AlphaArausalDetection(inEEGdata, SleepOnsetFlag);
		else
			AlphaAvgMean = HC_5_AlphaArausalDetection(inEEGdata, SleepOnsetFlag);

	}

	static bool HC_FLAG = 0;

	// ***** Initialization START ********************

	static bool initialized;

	//***** Filter
	// input buffer
	float static INPUTS[5];
	static int pow_counter = 0; // For moving average calculation

	//Related to Beta initialization

	static float Nbeta[5];
	static float Dbeta[5];

	float static betaFiltOut[5];

	static float betaPowerAccumulator=0;
	static float BetaAvgMean = 0;
	static float betaPower;
	static float betaPowerWindow[BETA_AVG_CALC_SAMP];

	static bool HC4_FLAG = 0;
	static float BETA_THR;


	static bool betaTH_aux = 0;


	// ***** Initialization ENDS ********************


	// ***** Get filtered data START *******************
	//Coefficient calculation
	if (!initialized) {
		float *Coeff;
		Coeff = CoeffCalculation(LowFcBeta, HighFcBeta);
		for (int i = 0; i < 5; i++){
			Nbeta[i] = Coeff[i];
			Dbeta[i] = Coeff[i+5];
			//printf("%s%f%s%f\n","N: ",Nbeta[i]," - D: ",Dbeta[i]);
		}
	    initialized = true;
	}

	// buffer shift
	for (int i = 1; i < 5; i++){
		INPUTS[5-i]        = INPUTS[4-i];// So loop with 5 steps is done in 1 period (1 sample!)
		betaFiltOut[5-i]   = betaFiltOut[4-i];
	}

	INPUTS[0]=inEEGdata;
	betaFiltOut[0]   = 0;

	// filter data
	betaFiltOut[0]   = Butter2ndOrder(INPUTS, betaFiltOut, Nbeta, Dbeta);

	// ***** Get filtered data ENDS ***********


	// ***** MVA calculation START ***********
	// Power calculation
	if ( (pow_counter > 0) & (pow_counter < BETA_POW_CALC_WIND) ){ //sum power values while counting up to BETA_POW_CALC_WIND seconds

		betaPowerAccumulator  += pow(betaFiltOut[0],2)/BETA_POW_CALC_WIND; // calculation of mean
		pow_counter+=1;

	}else if (pow_counter==0){ //initialize power value summations, start counting up to 1 second (after first after 1 has been reached)

		betaPowerAccumulator = pow(betaFiltOut[0],2)/BETA_POW_CALC_WIND;
		pow_counter+=1;

	}else{ //whenever counter has finished (at sample number fs), calculate the full average power

		pow_counter = 0; // Starts power calculation counter from 0 to again run until BETA_POW_CALC_WIND
		betaPower  = 0;
		//*** Compute moving window average of the BETA_POW_CALC_WIND_SEC average power values in an epoch of AVG_CALC_WIND_SEC seconds//
		betaPowerAccumulator += pow(betaFiltOut[0],2)/BETA_POW_CALC_WIND; // Defines power value for the current second by adding the last sample (see if else before)

		BetaAvgMean = betaPowerAccumulator;

		if (SleepOnsetFlag==1){ // If initial onset time.. calculate HC4_FLAG condition

			switch (ArausalDetCond){
			case 1:
				// HC4 condition Beta
				HC4_FLAG = HC4_BetaEvaluation(HC4_FLAG, BetaAvgMean, BETA_THR);
				HC_FLAG = HC4_FLAG;
				break;
			case 2:
				// HC5 condition
				HC5_FLAG = HC5_AlphaEvaluation(HC5_FLAG, BetaAvgMean, betaPower, AlphaAvgMean, alphaPower);
				HC_FLAG = HC5_FLAG;
				break;
			case 3:
				// HC4 and HC5 condition
				HC4_FLAG = HC4_BetaEvaluation(HC4_FLAG, BetaAvgMean, BETA_THR);
				HC5_FLAG = HC5_AlphaEvaluation(HC5_FLAG, BetaAvgMean, betaPower, AlphaAvgMean, alphaPower);
				HC_FLAG = HC4_FLAG|HC5_FLAG;
				break;
			default:
				break;

			}


		}else{
		// If sleep onset is not reached, calculate the average power in the last 10min
		// Buffer shift
			for (int i=1; i<BETA_AVG_CALC_SAMP;i++){
				betaPowerWindow[BETA_AVG_CALC_SAMP-i] = betaPowerWindow[BETA_AVG_CALC_SAMP-1-i];
			}

			betaPowerWindow[0] = BetaAvgMean*BETA_AVG_GAIN/(float)BETA_AVG_CALC_SAMP; // Actualization of the last number

			for (int i=0; i<BETA_AVG_CALC_SAMP;i++){
				betaPower  += betaPowerWindow[i]; // sums up last BETA_AVG_CALC_SAMP values
			}
			BETA_THR = betaPower*betaTH_PG;
			HC4_FLAG = 0;
			HC5_FLAG = 0;
		}
	}

	if (DEBUGGING_ON == 1){
		if (SAVE_HC4_BETA_FILT_OUT == 1)
			fprintf(f_HC4_FILT_OUT, "%f%s ", betaFiltOut[0], ",");
		if (SAVE_HC4_BETA_AVG_POW == 1)
			fprintf(f_HC4_AVG_POW, "%f%s ", BetaAvgMean, ",");
		if (SAVE_HARD_COND_4 == 1){
			fprintf(f_HARD_COND4, "%d%s ", HC4_FLAG, ",");
			if((SleepOnsetFlag==1)&&(betaTH_aux == 0)){
				fprintf(f_HC4_AVG_TH, "%f%s%f ", betaPower, ", ", BETA_THR);
				betaTH_aux = 1;
				printf("%s\n","Hard condition 4 enabled. Beta threshold calculated:");
				printf("%s%f\n", "  * 10min Average : ", betaPower);
				printf("%s%f\n\n", "  * Beta threshold: ", BETA_THR);
			}
		}

    	if (SAVE_HARD_COND_5 == 1)
			fprintf(f_HARD_COND5, "%d%s ", HC5_FLAG, ",");

	}

	return !HC_FLAG;
}





float HC_5_AlphaArausalDetection(float inEEGdata, int SleepOnsetFlag){

	// ***** Initialization START ********************

	static bool initialized;
	//***** Filter
	// coefficients
	static float Nalpha[5];
	static float Dalpha[5];
	static float AlphaMeanWind;

	// input buffer
	float static INPUTS[5];

	// output (data filtered) buffer
	float static alphaFiltOut[5];

	//**** for power and moving average calculation
	static int pow_counter = 0;
	static int pow_counter2 = 0;
	static float alphaPowerAccumulator=0;

	static float alphaPower;
	static float AlphaAvgPower;

	static float alphaPowerWindow[ALPHA_AVG_CALC_SAMP]; // sums up last 19 values, starts with 19, 18, 17... (only give 1 number since it is just summing up, shown by a + after var before 					deltaPower)

	// ***** Initialization ENDS ********************


	// ***** Get filtered data START *******************
	//Coefficient calculation
	if (!initialized) {
		float *Coeff;
		Coeff = CoeffCalculation(LowFcAlpha, HighFcAlpha);
		for (int i = 0; i < 5; i++){
			Nalpha[i] = Coeff[i];
			Dalpha[i] = Coeff[i+5];
			printf("%s%f%s%f\n","N: ",Nalpha[i]," - D: ",Dalpha[i]);
		}
	    initialized = true;
	}

	// buffer shift
	for (int i = 1; i < 5; i++){
		INPUTS[5-i]        = INPUTS[4-i];// So loop with 5 steps is done in 1 period (1 sample!)
		alphaFiltOut[5-i]  = alphaFiltOut[4-i];
	}

	INPUTS[0]=inEEGdata;
	alphaFiltOut[0]   = 0;

	// filter data
	alphaFiltOut[0]  = Butter2ndOrder(INPUTS, alphaFiltOut, Nalpha, Dalpha);

	// ***** Get filtered data ENDS ***********


	// ***** MVA calculation START ***********
	// Power calculation
	if ( (pow_counter > 0) & (pow_counter < ALPHA_POW_CALC_WIND) ){ //sum power values while counting up to ALPHA_POW_CALC_WIND seconds

		alphaPowerAccumulator += pow(alphaFiltOut[0],2)/ALPHA_POW_CALC_WIND; // calculation of mean
		pow_counter+=1;

	}else if (pow_counter==0){ //initialize power value summations, start counting up to 1 second (after first after 1 has been reached)

		alphaPowerAccumulator = pow(alphaFiltOut[0],2)/ALPHA_POW_CALC_WIND;
		pow_counter+=1;

	}else{ //whenever counter has finished (at sample number fs), calculate the full average power

		pow_counter = 0; // Starts power calculation counter from 0 to again run until ALPHA_POW_CALC_WIND
		alphaPower  = 0;

		//*** Compute moving window average of the ALPHA_POW_CALC_WIND_SEC average power values in an epoch of AVG_CALC_WIND_SEC seconds//
		alphaPowerAccumulator+= pow(alphaFiltOut[0],2)/ALPHA_POW_CALC_WIND; // Defines power value for the current second by adding the last sample (see if else before)
		AlphaMeanWind = alphaPowerAccumulator;

		if(SleepOnsetFlag==0){
			// Buffer shift
			for (int i=1; i<ALPHA_AVG_CALC_SAMP;i++){
				alphaPowerWindow[ALPHA_AVG_CALC_SAMP-i]= alphaPowerWindow[ALPHA_AVG_CALC_SAMP-1-i];
			}

			alphaPowerWindow[0] = alphaPowerAccumulator*ALPHA_AVG_GAIN/(float)ALPHA_AVG_CALC_SAMP; // Actualization of the last number

			for (int i=0; i<ALPHA_AVG_CALC_SAMP;i++){
				alphaPower += alphaPowerWindow[i]; // sums up last BETA_AVG_CALC_SAMP values
			}
			AlphaAvgPower = alphaPower;

		}else{
			AlphaAvgPower = AlphaMeanWind;
		}
	}

	if (DEBUGGING_ON == 1){
		if (SAVE_HC5_ALPHA_FILT_OUT == 1)
			fprintf(f_HC5_FILT_OUT, "%f%s ", alphaFiltOut[0], ",");

		if (SAVE_HC5_ALPHA_AVG_POW == 1)
			fprintf(f_HC5_AVG_POW, "%f ", AlphaMeanWind);
	}

	return AlphaAvgPower;

}


bool stim_cond_onoff_window(int STIM_CONDITION_HC2, int Restart, bool HC2_FLAG){

	bool HC2_wind_flag;
	static uint32_t time_counter = 0;

	if(HC2_FLAG==1){
		return HC2_FLAG;
	}else{

	if (Restart==1){
		time_counter  = 0;
		HC2_wind_flag = 1;
	}else{
		time_counter ++;
		if (time_counter<=STIM_ON_TIME_WIND)
			HC2_wind_flag = 0;
		else{
			if (time_counter<STIM_ON_TIME_WIND+STIM_OFF_TIME_WIND)
				HC2_wind_flag = 1;
			else{
				HC2_wind_flag = 1;
				time_counter = 0;
			}
		}
	}

	return HC2_wind_flag;
	}
}


bool HC_4_BetaArausalDetection(float inEEGdata, bool SleepOnsetFlag){

 	// ***** Initialization START ********************

 	static bool initialized;

 	//***** Filter
 	// coefficients
 	static float Nbeta[5];
 	static float Dbeta[5];

 	// input buffer
 	float static INPUTS[5];

 	// output (data filtered) buffer
 	float static betaFiltOut[5];

 	//**** for power and moving average calculation
 	static int pow_counter = 0;
 	static float betaPowerAccumulator=0;
 	static float BetaAvgMean = 0;

 	static float betaPower;

 	static float betaPowerWindow[BETA_AVG_CALC_SAMP];

 	static bool HC4_FLAG = 0;
 	static float BETA_THR;
 	static float onset_time_count = 0;

 	// ***** Initialization ENDS ********************


 	// ***** Get filtered data START *******************
 	//Coefficient calculation
 	if (!initialized) {
 		float *Coeff;
 		Coeff = CoeffCalculation(LowFcBeta, HighFcBeta);
 		for (int i = 0; i < 5; i++){
 			Nbeta[i] = Coeff[i];
 			Dbeta[i] = Coeff[i+5];
 			//printf("%s%f%s%f\n","N: ",Nbeta[i]," - D: ",Dbeta[i]);
 		}
 	    initialized = true;
 	}

 	// buffer shift
 	for (int i = 1; i < 5; i++){
 		INPUTS[5-i]        = INPUTS[4-i];// So loop with 5 steps is done in 1 period (1 sample!)
 		betaFiltOut[5-i]   = betaFiltOut[4-i];
 	}

 	INPUTS[0]=inEEGdata;
 	betaFiltOut[0]   = 0;

 	// filter data
 	betaFiltOut[0]   = Butter2ndOrder(INPUTS, betaFiltOut, Nbeta, Dbeta);

 	// ***** Get filtered data ENDS ***********


 	// ***** MVA calculation START ***********
 	// Power calculation
 	if ( (pow_counter > 0) & (pow_counter < BETA_POW_CALC_WIND) ){ //sum power values while counting up to BETA_POW_CALC_WIND seconds

 		betaPowerAccumulator  += pow(betaFiltOut[0],2)/BETA_POW_CALC_WIND; // calculation of mean
 		pow_counter+=1;

 	}else if (pow_counter==0){ //initialize power value summations, start counting up to 1 second (after first after 1 has been reached)

 		betaPowerAccumulator = pow(betaFiltOut[0],2)/BETA_POW_CALC_WIND;
 		pow_counter+=1;

 	}else{ //whenever counter has finished (at sample number fs), calculate the full average power

 		pow_counter = 0; // Starts power calculation counter from 0 to again run until BETA_POW_CALC_WIND
 		betaPower  = 0;
 		//*** Compute moving window average of the BETA_POW_CALC_WIND_SEC average power values in an epoch of AVG_CALC_WIND_SEC seconds//
 		betaPowerAccumulator += pow(betaFiltOut[0],2)/BETA_POW_CALC_WIND; // Defines power value for the current second by adding the last sample (see if else before)

 		BetaAvgMean = betaPowerAccumulator;

 		if (SleepOnsetFlag==1){ // If initial onset time.. calculate HC4_FLAG condition
 			if (HC4_FLAG==0){
 				if (BetaAvgMean<=BETA_THR){
 					HC4_FLAG = 0;
 				}else{
 					onset_time_count = 0;
 					HC4_FLAG = 1;
 				}
 			}else{
 				onset_time_count++;
 				if (onset_time_count==HC4_ONSET_TIME)
 					HC4_FLAG = 0;
 				else
 					HC4_FLAG = 1;
 			}
 		}else{
 		// If sleep onset is not reached, calculate the average power in the last 10min
 		// Buffer shift
 			for (int i=1; i<BETA_AVG_CALC_SAMP;i++){
 				betaPowerWindow[BETA_AVG_CALC_SAMP-i] = betaPowerWindow[BETA_AVG_CALC_SAMP-1-i];
 			}

 			betaPowerWindow[0] = BetaAvgMean*BETA_AVG_GAIN/(float)BETA_AVG_CALC_SAMP; // Actualization of the last number

 			for (int i=0; i<BETA_AVG_CALC_SAMP;i++){
 				//betaPower  += betaPowerWindow[BETA_AVG_CALC_SAMP-i]; // sums up last BETA_AVG_CALC_SAMP values
 				betaPower  += betaPowerWindow[i]; // sums up last BETA_AVG_CALC_SAMP values
 			}
 			BETA_THR = betaPower*betaTH_PG;
 			HC4_FLAG = 0;
 		}

 	}
 	if (DEBUGGING_ON == 1){
 	if (SAVE_HARD_COND_4 == 1){
 			fprintf(f_HARD_COND4, "%d%s ", HC4_FLAG, ",");
 	}
 	}

 	return !HC4_FLAG;
 }



bool HC6_PLL(float PLL_INPUT, bool INIT_COND_PLL, int PLL_flag, float SWfilt){ // Two conditions: 1) raw data < 0. 2) Negative slope

	bool HC6_PLL_flag;
	static uint8_t counter = 0;
	static float PLL_input_prev = 0;
	float SlopeInput = PLL_INPUT; // Or PLL_INPUT in case of raw data
	static float Ampdiff = 0;

	static bool BuffMem[3];
	static bool slope = 0;
	static bool AmpThFlag = 0;
	static float prev_PLL_INPUT = 0;

	if (INIT_COND_PLL==1){
		counter = 0;
		PLL_input_prev = 0;
		HC6_PLL_flag = 0;
		Ampdiff = 0;
		slope = 0;
		for (int i = 0; i < 3; i++)
			BuffMem[i] = 0.0;
	}

	// Detection of Slope direction
	if (counter == SAMPLES_PER_SLOPE){

		Ampdiff = abs(SlopeInput - PLL_input_prev);
		slope = (SlopeInput - PLL_input_prev)>=0;

		for (int i = 0; i < 3-1; i++)
			BuffMem[i] = BuffMem[i+1];
		BuffMem[2] = slope;

		counter = 1;
		PLL_input_prev = SlopeInput; //PLL_INPUT;

	}else{
		counter++;
	}


//	if (PLL_INPUT > HC6_THRESHOLD)
//		AmpThFlag = 1;
//	if ((prev_PLL_INPUT>0)&&(PLL_INPUT<0)) // Negative cross zero
//		AmpThFlag = 0;

	if (PLL_flag == 1){
		if	(PLL_INPUT<0)
			HC6_PLL_flag = 0;
		else{

			// Case with memory
//			if((BuffMem[2]==1)&&(BuffMem[1]==1))
//				HC6_PLL_flag = 1;
//			else
//				HC6_PLL_flag = 0;

			// Case without memory
//			if (slope==0) // Negative slope //PLL_INPUT
//				HC6_PLL_flag = 0;
//			else
//				HC6_PLL_flag = 1;

			// Case with amplitude threshold
//			if ((PLL_INPUT < HC6_THRESHOLD)&&(AmpThFlag==1))
//				HC6_PLL_flag = 0;
//			else
//				HC6_PLL_flag = 1;

		}
	}else{
		HC6_PLL_flag = 0;
	}

 	if (DEBUGGING_ON == 1){
 		if (SAVE_HARD_COND_6 == 1){
 			fprintf(f_HARD_COND6, "%d%s ", !HC6_PLL_flag, ",");
 		}
 	}

	return HC6_PLL_flag;

}

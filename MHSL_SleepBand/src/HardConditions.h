/*
 * ==========================================================================================================
 * Name        : HardConditions.h
 * Author      : Maria Laura Ferster
 * Version     : 1.0
 * Date		   : 07-January-2018
 * Copyright   : (c) 2018.  Maria Laura Ferster, Mobile Health Systems Lab, ETH Zurich. All rights reserved
 *
 * Description : Conditions that needs to be satisfied in order to stimulate.
 *
 * Objective   : Avoid false stimulations
 ============================================================================================================
 */
#ifndef HARDCONDITIONS_H
#define HARDCONDITIONS_H

#include "config.h"
#include "main.h"
#include "filters.h"


bool HC_0_SleepOnsetDelayCondition(uint32_t counter, bool INIT_SLEEP_ONSET_FLAG);
bool HC_1_RemSleepOnsetDelayCondition(uint32_t counter, bool INIT_F);
int HC_2_ConsecutiveTones(int STIM_CONDITION_HC2, int SWD_FLAG, int INIT_flag, int Restart, int actual_tone_time);
bool HC_3_MovementDetection_FromRawData(float EEG_raw);

bool HC_AraulsaDetection(float inEEGdata, int SleepOnsetFla, int ArausalDetCond);
float HC_5_AlphaArausalDetection(float inEEGdata, int SleepOnsetFla);

bool HC5_AlphaEvaluation(bool HC5_FLAG, float BetaAvgMean, float betaPower, float AlphaAvgMean, float alphaPower);
bool HC4_BetaEvaluation(bool HC4_FLAG, float BetaAvgMean, float BETA_THR);

bool HC_4_BetaArausalDetection(float inEEGdata, bool SleepOnsetFlag);

bool stim_cond_onoff_window(int STIM_CONDITION_HC2, int Restart, bool HC2_FLAG);

bool HC6_PLL(float PLL_INPUT, bool INIT_COND_PLL, int PLL_flag, float SWfilt);

int BetaPowerCalc(float inEEGdata);
int AlphaPowerCalc(float inEEGdata);
#endif //!defined

/*
 * ==========================================================================================================
 * Name        : PLL.c
 * Author      : Maria Laura Ferster
 * Version     : 1.0
 * Date		   : 12-June-2017
 * Copyright   : (c) 2017.  Maria Laura Ferster, Mobile Health Systems Lab, ETH Zurich. All rights reserved
 *
 * Description : Phase-Locked Loop for slow wave phase tracking.
 *
 * Objective   : Detect the phase of the slow wave to stimulation at a particular target phase.
 *
 ============================================================================================================
 */
	
#include "PLL.h"
#include "utils.h"
#include "HardConditions.h"

float WrapToPi(float value){

	value = fmod(value + (float)M_PI,(float)M_2PI);

	if (value < 0)
		value += (float)M_2PI;

	return value - (float)M_PI;

}

float HighPassFilterT_01Hz(float data, bool INI_COND_PLL){

    float RC = 1.0/(HIGHPASS_FREQ*(float)M_2PI);
    float dt = 1.0/SAMPLE_FREQ;
    float alpha = RC/(RC + dt);
    static float filteredData;
    static float prev_data = 0.0;

    if (INI_COND_PLL==1){prev_data = 0;}

    filteredData = alpha * (filteredData + data - prev_data);
    prev_data = data;
    return filteredData;
}


float EEG_preprocessing(float PLL_input_raw){

	 /*static double NumDen[6] = {1.0, -2.0, 1.0, 1.0, -1.9988788503381982, 0.99887947847446157};
	 static double Gain = 0.99943958220316498;
	 static double Out_Gain = 1.0;
	 static double Buff[3];
	 double S;*/

	 static float NumDen[6] = {1.0, -2.0, 1.0, 1.0, -1.9988788503381982, 0.99887947847446157};
	 static float Gain = 0.99943958220316498;
 	 static float Out_Gain = 1.0;
 	 static float Buff[3];
 	 float S;

     Buff[2] = Gain*PLL_input_raw - NumDen[4]*Buff[1] - NumDen[5]*Buff[0];
     S = Buff[2]*NumDen[2] + Buff[0]*NumDen[0] + Buff[1]*NumDen[1];

 	for (int i = 0; i < 3; i++){
 		Buff[i] = Buff[i + 1];
 	}

 	return  Out_Gain*(float)S;
}


int HC_PLL(float PLL_input_raw, bool INIT_COND_PLL, float SW_Filt_data, int STIMULATION, int SWD, int SD, bool HC2_F) {

	float PLL_input;
	float PLL_arg_diff;

	// PLL Variables to keep in memory
	static float NCO = 0;
	static float PHASE_DETECTOR = 0;
	static float PhaseEstimation = 0;
	static float PLL_ARGUMENT = 0;
	static float PLL_ARGUMENT_prev = 0;
	static uint32_t n = 2;
	static uint32_t ITI_counter = 0;
	static int return_value = 0;
	static int first_tone_flag = 0;
	static int PLL_tone = 0;

	static float PROPORTIONAL = 0;
	static float INTEGRATOR = 0;
	static float PI_controller = 0;
	static float PHASE_ERROR = 0;
	static float PHASE_ERROR_N_1 = 0;

	if (INIT_COND_PLL==1){
		NCO = 0;
		PHASE_DETECTOR = 0;
		PhaseEstimation = 0;
		PLL_ARGUMENT = 0;
		PLL_ARGUMENT_prev = 0;
		return_value = 0;
		n = 2;
		PROPORTIONAL = 0;
		INTEGRATOR = 0;
		PI_controller = 0;
		PHASE_ERROR = 0;
		PHASE_ERROR_N_1 = 0;
	}


	if (LOAD_DATA_FROM_KISPI==1)
		PLL_input = PLL_input_raw;
	else
		PLL_input = HighPassFilterT_01Hz(PLL_input_raw, INIT_COND_PLL);

	NCO = Wc * n + PhaseEstimation; 				// Compute NCO signal

	PLL_input = PLL_input*PLL_INPUT_AMP;

	PHASE_DETECTOR = PLL_input * sin(NCO) * (GAIN_PD / 1000000.0); // Complex multiply nco x Signal input
	PhaseEstimation = WrapToPi(PhaseEstimation - PHASE_DETECTOR * GAIN_NCO);// Phase between pi and -pi

	PLL_ARGUMENT = WrapToPi(Wc * n + PhaseEstimation);
	PLL_arg_diff = fabs(PLL_ARGUMENT - PLL_ARGUMENT_prev);

	if ((PLL_ARGUMENT_prev < (float)TARGET_PHASE) & (PLL_ARGUMENT >= (float)TARGET_PHASE) & (PLL_arg_diff < (float)M_PI_2)){ // Target phase is reached
		return_value = 1;
	}else{
		return_value = 0;
	}

	if (PLL_input < 0)  // HC6 avoiding negative stimulations
		return_value = 0;

	//if (ENABLE_HARD_COND_6==1){return_value = return_value*HC6_PLL(PLL_input, INIT_COND_PLL, return_value, SW_Filt_data);}

	PLL_ARGUMENT_prev = PLL_ARGUMENT;

	n++;



	if(STIM_METHOD == 1){

		if ((INIT_COND_PLL==1)||(SWD==0)||(SD==0)||(HC2_F==1)){
			first_tone_flag = 0;
			ITI_counter = 0;
		}

		if (first_tone_flag==1){
			PLL_tone = 0;
			ITI_counter++;
			if(ITI_counter>=INTER_TONE_INTERVAL){
				return_value = 1;
				ITI_counter = 0;
			}
			else
				return_value =  0;
		}else{
			if (return_value==1){
				first_tone_flag = 1;
				PLL_tone = 1;
			}
			else{
				first_tone_flag = 0;
			}
		}
	}



	if (SAVE_PLL_ARGUMENT == 1)
		fprintf(f_PLL_ARG, "%f%s%f%s", PLL_ARGUMENT, ", ", PHASE_ERROR, ", ");
		//fprintf(f_PLL_ARG, "%f%s", PLL_ARGUMENT, ", ");
		//fprintf(f_PLL_ARG, "%d%s", PLL_tone, ", ");
	if (SAVE_PLL_FILTERED_DATA == 1)
		fprintf(f_PLL_INP, "%d%s", PLL_tone, ", ");

	return return_value>0;
}


/*
 * ==========================================================================================================
 * Name        : PLL.h
 * Author      : Maria Laura Ferster
 * Version     : 1.0
 * Date		   : 13-June-2017
 * Copyright   : (c) 2017.  Maria Laura Ferster, Mobile Health Systems Lab, ETH Zurich. All rights reserved
 ============================================================================================================
 */

#ifndef PLL_H 
#define PLL_H

#include "main.h"

float WrapToPi(float value);
int HC_PLL(float PLL_input_raw, bool INIT_COND_PLL, float SW_Filt_data, int STIMULATION, int SWD, int SD, bool HC2_F);
float EEG_preprocessing(float PLL_input_raw);
float HighPassFilterT_01Hz(float data, bool INI_COND_PLL);
float ButterworthFilter(float INPUT_data);
float BandPassFilter(float INPUT_data);
float LowPassFilterT_Fc5Hz(float data, bool INI_COND_PLL);

#endif //!defined

/*
 * ==========================================================================================================
 * Name        : SW_detector.c
 * Author      : Maria Laura Ferster
 * Version     : 1.0
 * Date		   : 12-June-2017
 * Copyright   : (c) 2017.  Maria Laura Ferster, Mobile Health Systems Lab, ETH Zurich. All rights reserved
 *
 * Description : Slow wave detection function. Detects slow waves presence based on 1) SW amplitude and
 * 		frequency characteristics.
 *
 * Objective   : Detect the Slow waves presence and toghether with the PLL stimulate at a particular SW target
 * 		phase.
 *
 ============================================================================================================
 */
	
#include "SW_detector.h"
#include "filters.h"


float SO_filtering(float inRawData){

	float static raw_input_buffer[SO_CoeffOrder];
	float static filterOut_buffer[SO_CoeffOrder];

	// Filter the input data
	static bool initialized;

	// coefficients
	static float N_SO[SO_CoeffOrder];
	static float D_SO[SO_CoeffOrder];


	if (!initialized) {
		float *Coeff;
		//Coeff = CoeffCalculation(LowFcSO, HighFcSO);
		Coeff = FLT_CoeffCalc(SO_FcLow, SO_FcHigh, SO_Order);
		for (int i = 0; i < SO_CoeffOrder; i++){
			N_SO[i] = Coeff[i];
			D_SO[i] = Coeff[i+SO_CoeffOrder];
			printf("%s%f%s%f \n", "N_OS: ",N_SO[i], " - D_OS: ",D_SO[i]);
		}
		initialized = true;
	}

	// buffer shift
	for (int i = 1; i < SO_CoeffOrder; i++){
		raw_input_buffer[SO_CoeffOrder-i] = raw_input_buffer[SO_CoeffOrder-1-i];// So loop with 5 steps is done in 1 period (1 sample!)
		filterOut_buffer[SO_CoeffOrder-i] = filterOut_buffer[SO_CoeffOrder-1-i];// So loop with 5 steps is done in 1 period (1 sample!)
	}
	raw_input_buffer[0] = inRawData;
	filterOut_buffer[0] = 0;

	filterOut_buffer[0] = ButterN_Order(raw_input_buffer, filterOut_buffer, N_SO, D_SO, SO_Order);

	return filterOut_buffer[0];
	//return filterOut;
}


int SlowWaveDetector(float INPUT_data, int filter_delay, bool INIT_COND, float SWD_Filt_out){

	int SWD_FLAG = 0;

	if (SWD_METHOD==0)
		SWD_FLAG = SW_detection_Freq_and_Amp(INPUT_data, filter_delay, INIT_COND);
	else
		SWD_FLAG = SW_detection_Power(INPUT_data, filter_delay, INIT_COND, SWD_Filt_out);

	return SWD_FLAG;
}





int SW_detection_Freq_and_Amp(float INPUT_data, int filter_delay, bool INIT_COND){

	static float prev_sample = 0;
	//float actual_sample = SO_filtering(INPUT_data);
	float actual_sample = SO_KISPI_filter(INPUT_data, INIT_COND);
	//float actual_sample = SO_MHSL_filter(INPUT_data);

	float data_at_TH = 0;
	// Flags for SW detection conditions
	static int DOWN_ZERO_CROSSING  = 0;
	static int UP_ZERO_CROSSING    = 0;
	static int THRESHOLD_DETECT    = 0;
	static int UP_THRESHOLD_DETECT = 0;
	static int SAMPLE_COUNTER 	   = 0;
	static int SW_DETECTOR_FLAG    = 0; // return value

	if (INIT_COND == 1){
		DOWN_ZERO_CROSSING  = 0;
		UP_ZERO_CROSSING    = 0;
		THRESHOLD_DETECT    = 0;
		UP_THRESHOLD_DETECT = 0;
		SAMPLE_COUNTER 	    = 0;
		SW_DETECTOR_FLAG    = 0; // return value
		prev_sample = 0;
	}

	//if ((filter_delay >= (SO_FILTERING_DELAY))&&(tone_counter<CONSECUTIVE_TONES_NUM)){
	if (filter_delay >= (SO_FILTERING_DELAY)){

	    if  (( prev_sample > 0)  &&  (actual_sample < 0)){
	        DOWN_ZERO_CROSSING = 1;
	        UP_ZERO_CROSSING   = 0;


	    }else{//Detection of up zero crossing
	    	if ( (prev_sample < 0)  &&  (actual_sample > 0) && (DOWN_ZERO_CROSSING == 1) ){
	    		DOWN_ZERO_CROSSING = 0;
	    		UP_ZERO_CROSSING   = 1;

	    		// Check the half-period condition for SW detection
	    		// Half period condition between ((1/2Hz)/2)*1e3 ms and ((1/0.5Hz)/2)*1e3 ms
	    		if ( (FREQ_CONDITION_LOW_LIM <= SAMPLE_COUNTER)  && (SAMPLE_COUNTER <= FREQ_CONDITION_HIGH_LIM) ){

	    			//Half period condition satisfied. Check minimum threshold
	    			if ((THRESHOLD_DETECT == 1) &&(UP_THRESHOLD_DETECT == 0)){
	    				SW_DETECTOR_FLAG = 1; // Threshold and upper amplitude condition satisfied
	    			}else{
	    				SW_DETECTOR_FLAG = 0;
	    			}
	    		}
	    		SAMPLE_COUNTER = 0;
	    		UP_THRESHOLD_DETECT = 0;

	    	}else{
	    		if  ((THRESHOLD_DETECT == 1) && (DOWN_ZERO_CROSSING == 0)){ // Make sure that if threshold are not fullfilled threshold is set to 0 to avoid stim detection later on
	    	       THRESHOLD_DETECT = 0;
	    	       SAMPLE_COUNTER = 0;
	    	       UP_THRESHOLD_DETECT = 0;
	    		}
	    	}
	    }


	    // negative amplitude detection
	    if (DOWN_ZERO_CROSSING == 1){
	        SAMPLE_COUNTER  = SAMPLE_COUNTER+1;	// Start the sample counter for half a period condition.
	        if  ( (THRESHOLD_DETECT == 0) && (actual_sample <= SW_THRESHOLD) )
	            THRESHOLD_DETECT = 1;
	    }

	    // If SW detected during negative part, flag is on during all positive part
	    if ((UP_ZERO_CROSSING == 1) && (SW_DETECTOR_FLAG == 1))
	    	SW_DETECTOR_FLAG = 1;
	    else
	    	SW_DETECTOR_FLAG = 0;

	}else{

		SW_DETECTOR_FLAG = 0;
	}


	if (DEBUGGING_ON == 1){
		if (SAVE_SW_TH == 1)
			fprintf(f_SW_TH, "%f%s ", data_at_TH, ",");
		if (SAVE_SW_FILTERED_DATA == 1)
			fprintf(f_SW_FILT, "%f%s ", actual_sample, ",");

	}

	prev_sample = actual_sample;

	return SW_DETECTOR_FLAG;
}




float SWD_Power_FiltData(float INPUT_data){

	static bool initialized;

	//generate transfer function filter coefficients
	//***** Filter
	float static INPUTS[5]; // input buffer
	static int pow_counter = 0; // For moving average calculation

	//Related to delta initialization
	static float Ndelta[5];
	static float Ddelta[5];

	float static DeltaFiltOut[5];

	static float DeltaPowerAccumulator=0;
	static float DeltaPower;
	static float DeltaPowerWindow[SWD_PWR_mvaLength];

	// ***** Get filtered data START *******************
	//Coefficient calculation
	if (!initialized) {
		float *Coeff;
		Coeff = CoeffCalculation(SWD_PWR_deltaFiltLow, SWD_PWR_deltaFiltHi);
		for (int i = 0; i < 5; i++){
			Ndelta[i] = Coeff[i];
			Ddelta[i] = Coeff[i+5];
		}
	    initialized = true;
	}

	// buffer shift
	for (int i = 1; i < 5; i++){
		INPUTS[5-i]        = INPUTS[4-i];// So loop with 5 steps is done in 1 period (1 sample!)
		DeltaFiltOut[5-i]  = DeltaFiltOut[4-i];
	}

	INPUTS[0]=INPUT_data;
	DeltaFiltOut[0]   = 0;

	// filter data
	DeltaFiltOut[0] = Butter2ndOrder(INPUTS, DeltaFiltOut, Ndelta, Ddelta);

	return DeltaFiltOut[0];
}




int SW_detection_Power(float INPUT_data, int filter_delay, bool INIT_COND, float SWD_Filt_out){


	static int SWD_POW_FLAG = 0; // return value
	static int pow_counter = 0; // For moving average calculation

	static float DeltaPowerAccumulator=0;
	static float DeltaPower;
	static float DeltaPowerWindow[SWD_PWR_mvaLength];

	// When initialization condition == 1, start over again
	if (INIT_COND == 1){
		DeltaPowerAccumulator = 0;
		DeltaPower 			  = 0;
		pow_counter 		  = 0;
	}

    //calcualte 1s static averages of filtered signal
	// ***** MVA calculation START ***********
	// Power calculation
	if ( (pow_counter > 0) & (pow_counter < SWD_PWR_PowWind - 1) ){ //sum power values while counting up to POW_CALC_WIND seconds

		DeltaPowerAccumulator  += pow(SWD_Filt_out,2)/SWD_PWR_PowWind;
		pow_counter+=1;

	}else if ((pow_counter==0)&&(SWD_PWR_PowWind>1)){ //initialize power value summations, start counting up to 1 second (after first after 1 has been reached)

		DeltaPowerAccumulator = pow(SWD_Filt_out,2)/SWD_PWR_PowWind;
		pow_counter+=1;

	}else{ //whenever counter has finished (at sample number fs), calculate the full average power

		pow_counter = 0; // Starts power calculation counter from 0 to again run until SWD_PWR_PowWind
		DeltaPower  = 0;

		//*** Compute moving window average of the POW_CALC_WIND_SEC average power values in an epoch of AVG_CALC_WIND_SEC seconds//
		if (SWD_PWR_PowWind>1)
			DeltaPowerAccumulator += pow(SWD_Filt_out,2)/SWD_PWR_PowWind;
		else
			DeltaPowerAccumulator = pow(SWD_Filt_out,2)/SWD_PWR_PowWind;

		// Buffer shift
		for (int i=1; i<SWD_PWR_mvaLength;i++){
			DeltaPowerWindow[SWD_PWR_mvaLength-i] = DeltaPowerWindow[SWD_PWR_mvaLength-1-i];
		}

		DeltaPowerWindow[0] = DeltaPowerAccumulator/(float)SWD_PWR_mvaLength;

		for (int i=1; i<=SWD_PWR_mvaLength;i++){
			DeltaPower  += DeltaPowerWindow[SWD_PWR_mvaLength-i]; // Line 274 replaced DeltaPowerWindowS with DeltaPowerWindow (no S)
		}

	}

	//printf("%f\n",DeltaPowerAccumulator);

	if (filter_delay <=0){ // due to HC3
		SWD_POW_FLAG = 0;
	}else{
		if (DeltaPower >= SWD_PWR_THRESHOLD)
			SWD_POW_FLAG = 1;
		else
			SWD_POW_FLAG = 0;

	}



	if (DEBUGGING_ON == 1){
		if (SAVE_SW_FILTERED_DATA == 1)
			fprintf(f_SW_FILT, "%f%s ", DeltaPower, ",");
	}


	return SWD_POW_FLAG;
}

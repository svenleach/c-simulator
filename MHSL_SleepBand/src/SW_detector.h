/*
 * ==========================================================================================================
 * Name        : SW_detector.h
 * Author      : Maria Laura Ferster
 * Version     : 1.0
 * Date		   : 12-June-2017
 * Copyright   : (c) 2017.  Maria Laura Ferster, Mobile Health Systems Lab, ETH Zurich. All rights reserved
 ============================================================================================================
 */

#ifndef SW_DETECTOR_H 
#define SW_DETECTOR_H

#include "main.h"

float SO_filtering(float inRawData);
int SW_detection_Freq(float *INPUT);
int SlowWaveDetector(float INPUT_data, int filter_delay, bool INIT_COND, float SWD_Filt_out);
int SW_detection_Freq_and_Amp(float INPUT_data, int filter_delay, bool INIT_COND);
int SW_detection_Power(float INPUT_data, int filter_delay, bool INIT_COND, float SWD_Filt_out);
float SWD_Power_FiltData(float INPUT_data);
  //int delete_vectors();
  

#endif //!defined 

/*
 * ==========================================================================================================
 * Name        : VolumeControl.c
 * Author      : Maria Laura Ferster
 * Version     : 1.0
 * Date		   : 19-March-2018
 * Copyright   : (c) 2018.  Maria Laura Ferster, Mobile Health Systems Lab, ETH Zurich. All rights reserved
 *
 * Description : Controls the volume based on arousal detection after tone has been played
 *
 * ============================================================================================================
 */

#include "VolumeControl.h"
#include "utils.h"

int VolumeCtrlArousal(int Sleep, int Tone, bool Arousal){


	bool Arousal_det = !Arousal;

	static int volume = INITIAL_VOLUME;
	static int counter = 0;
	static bool start_counter = 0;
	static int tone_counter 	= 0;
	static int arousal_counter = 0;
	static int last_tone_counter_A 	= 0;
	static int tone_counter_A 	= 0;



	static int DELTA2_TH;
	if (POPULATION==0){
		DELTA2_TH = VC_INCREASE_VOL_DELTA2_Y;
	}else{
		DELTA2_TH = VC_INCREASE_VOL_DELTA2_E;
	}
	static float Delat2_power;
	Delat2_power = NREM_Delta2_Transfer(0.0, 1);

	if (SAVE_SD_POW_D2_VC == 1)
		fprintf(f_POW_D2_VC, "%f%s ", Delat2_power, ",");

	if (Sleep==0){ // Initialize everything to zero
		volume 			 	= 0;
		counter 		 	= 0;
		start_counter 	 	= 0;
		tone_counter 	 	= 0;
		arousal_counter  	= 0;
		last_tone_counter_A	= 0;
		tone_counter_A 	 	= 0;

	}else{

		if (Tone == 1){  		// When tone is played, start a counter to detect whether an arousal happens during the AROUSAL_TONE_TIME after tone

			counter 		= 0;		// every time a tone is played, start the counter
			start_counter 	= 1;
			tone_counter++;
			tone_counter_A++;

			if (volume < MINIMUM_VOLUME)					// If deep sleep is detected, starts with default volume setting (or maybe last one?)
				volume = INITIAL_VOLUME;
		}

		// Analyze data after tone
		if (start_counter==1){
			counter++; // counter of time
			if (counter>= AROUSAL_TONE_TIME){ // Analyze data during AROUSAL_TONE_TIME. After arousal time finished(no other tone detected), start counting arousal from zero
				//start_counter = 0;
				arousal_counter = 0;
			}

			if (Arousal_det==1){ // If arousal detected, check whether is a related to tone and adjust volume

				int tone_A_diff = last_tone_counter_A - tone_counter_A;
				if(tone_A_diff<=VC_TONES_BETWEEN_AROUSALS)
					arousal_counter++;
				else
					arousal_counter=1;

				if ((volume>=MINIMUM_VOLUME+VOLUME_STEP)&&(arousal_counter == VC_CONSECUTIVE_AROUS)){
					volume = volume - VOLUME_STEP;	// Arousal related to tone detected
					arousal_counter = 0;
				}

				tone_counter = 0;
				start_counter = 0;
				last_tone_counter_A = tone_counter_A;

			}else{				// No arousal detected, increase the tone

					if (( volume<=MAXIMUM_VOLUME-VOLUME_STEP )&&(tone_counter == VC_CONSECUTIVE_TONES)){
						volume = volume + VOLUME_STEP;
						tone_counter = 0;
						arousal_counter = 0;
					}

					if(Delat2_power< DELTA2_TH){
						volume = INITIAL_VOLUME;
						tone_counter = 0;
						arousal_counter = 0;
					}
			}
		}
	}

	return volume;
}



/*
 * ==========================================================================================================
 * Name        : VolumeControl.h
 * Author      : Maria Laura Ferster
 * Version     : 1.0
 * Date		   : 19-March-2018
 * Copyright   : (c) 2018.  Maria Laura Ferster, Mobile Health Systems Lab, ETH Zurich. All rights reserved
 *
 * Description : Controls the volume based on arousal detection after tone has been played
 *
 * ============================================================================================================
 */

#ifndef VOLUMECONTROL_H_
#define VOLUMECONTROL_H_

#include "config.h"
#include "main.h"

int VolumeCtrlArousal(int Sleep, int Tone, bool Arousal);

#endif /* VOLUMECONTROL_H_ */

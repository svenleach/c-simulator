/*
 * config.h
 *
 *  Created on: 23.06.2017
 *      Author: fersterm
 */

#ifndef CONFIG_H_
#define CONFIG_H_

/******************************/
/* General definition */
/******************************/
#define LOAD_DATA_FROM_KISPI 	0
#define SAMPLE_FREQ   			(250*(1+LOAD_DATA_FROM_KISPI))



/******************************/
/*     Phase-locked loop      */
/******************************/
#define GAIN_PD   		1000
#define GAIN_NCO   		0.095
#define TARGET_PHASE   	(-M_PI/4)
#define CENTRAL_FREQ 	1		// Central NCO frequency at 1Hz
#define Wc 				(2*M_PI*CENTRAL_FREQ/SAMPLE_FREQ)
#define HIGHPASS_FREQ	0.1


/******************************/
/* SLOW WAVE DETECTOR         */
/******************************/
#define SW_THRESHOLD 				-40 //-10000//-40
#define SW_UP_THRESHOLD 			-300 //-3000000//-300				// If wave is smaller than this threshold, then it is considered an arausal
#define FREQ_CONDITION_LOW_LIM  	0.5*SAMPLE_FREQ/2 	// Half period of a 0.5Hz wave
#define FREQ_CONDITION_HIGH_LIM 	2*SAMPLE_FREQ/2 	// Half period of a 2Hz wave

#define SWD_WITH_UP_TH				1
#define SWD_FREQ_UP_COND			1


// Slow Oscilation (SO) cutoff frequencies
// For kispi filter use hard filter configuration. Seel filters.h
#define LowFcSO_KISPI				0.319825
#define HighFcSO_KISPI				3.12648

#define SO_FcLow					0.2
#define SO_FcHigh					5
#define SO_Order					2
#define SO_CoeffOrder				(2*SO_Order)+1

// After detecting Sleep, wait until NREM sleep is stable
#define SW_DETECTION_DELAY_IN_SEC	0//(60*3-10)
#define SW_DETECTION_DELAY			SW_DETECTION_DELAY_IN_SEC*SAMPLE_FREQ
#define SO_FILTERING_DELAY			0//10*SAMPLE_FREQ

/******************************/
/*Seep detection by Caroline*/
/******************************/
// Frequency bands
#define LowFcDelta2			2
#define HighFcDelta2		4
#define LowFcDelta4			3
#define HighFcDelta4		5
#define LowFcMuscPow2		20 // only if second order filter is used
#define HighFcMuscPow2		30 // only if second order filter is used


//config for 80 sec epoch
#define POW_CALC_WIND_SEC 	4
#define POW_CALC_WIND     	(POW_CALC_WIND_SEC*SAMPLE_FREQ)
#define AVG_CALC_WIND_SEC 	80
#define AVG_CALC_SAMP  		(AVG_CALC_WIND_SEC/POW_CALC_WIND_SEC)
#define AVG_GAIN			4


// Thresholds according to population: "0": for young adults. "1": for elderly
#define POPULATION 			0

// Elderly
#define E_D4_OVER_B2_TH 	4.5
#define E_B2_TH				24
#define E_Muscle_TH			16

// Young adults
#define Y_D4_OVER_M_TH 		8
#define Y_D2_TH				176
#define Y_Muscle_TH			20


/******************************/
/*         AUDIO              */
/******************************/

#define TONE_DURATION		0.05  // 50 ms duration
#define AUDIO_FREQUENCY		16000 // 16 KHZ
#define AUDIO_VOLUME		35
#define AUDIO_BUFFER_SIZE   4096
#define TONE_TYPE			"PINK_NOISE"

#define TONE_DURATION_IN_SAMP	TONE_DURATION*AUDIO_FREQUENCY


#endif /* CONFIG_H_ */




















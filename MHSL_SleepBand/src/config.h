/*
 * ==========================================================================================================
 * Name        : config.h
 * Author      : Maria Laura Ferster
 * Version     : 1.0
 * Date		   : 23-June-2017
 * Copyright   : (c) 2017.  Maria Laura Ferster, Mobile Health Systems Lab, ETH Zurich. All rights reserved
 *
 * Description : All configuration parameters are contained in this file
 *
 * Objective   : Modify algorithms parameters through one single file. This file will be the only one
 * 		visible and accessible for hardware users
 ============================================================================================================
 */

#ifndef CONFIG_H_
#define CONFIG_H_

/******************************/
/* General definition */
/******************************/
#define LOAD_DATA_FROM_KISPI 	0
#define SAMPLE_FREQ   			(250*(1+LOAD_DATA_FROM_KISPI))


/******************************/
/*  Main configuration		  */
/******************************/
// 0: all slow waves detected
// 1: ON-OFF burst of tones
#define STIM_COND			1

#define GAIN_NCO   1
#define GAIN_PD   3200


#define SWD_PWR_THRESHOLD   800

// Thresholds according to population: "0": for young adults. "1": for elderly
#define POPULATION 			0

// Elderly thresholds
#define E_D4_OVER_M_TH	 	8
#define E_D2_TH				450
#define E_Muscle_TH			60

#define ENABLE_HARD_COND_0	1
#define ENABLE_HARD_COND_1	1
#define ENABLE_HARD_COND_2	1   // Still need to be tested
#define ENABLE_HARD_COND_3	1
#define ENABLE_HARD_COND_4	1  	// beta condition
#define ENABLE_HARD_COND_6	1  	// PLL condition


/******************************/
/*     Phase-locked loop      */
/******************************/
#define TARGET_PHASE   -0.15
#define CENTRAL_FREQ 	1			// Central NCO frequency at 1Hz
#define Wc 				(2*M_PI*CENTRAL_FREQ/SAMPLE_FREQ)
#define HIGHPASS_FREQ	0.1
#define PLL_INPUT_AMP	2

/******************************/
/* SLOW WAVE DETECTOR         */
/******************************/
#define SWD_METHOD	1 // SWD_METHOD=0: Amplitude and frequency method; SWD_METHOD=1: Power method.

//---------------------------------------------------------
// * SWD_METHOD = 0;
// * Amplitude and frequency condition Slow Wave detection
// * SWD_METHOD	must be 0 to enable this method
#define SW_THRESHOLD 				-30							// Amplidure threshold in uVolts
#define FREQ_LOW					 0.5						// in Hz
#define FREQ_HIGH		 		     2							// in Hz
#define FREQ_CONDITION_LOW_LIM  	 FREQ_LOW*SAMPLE_FREQ/2 	// Half period of a 0.5Hz wave
#define FREQ_CONDITION_HIGH_LIM 	 FREQ_HIGH*SAMPLE_FREQ/2 	// Half period of a 2Hz wave


// Slow Oscilation (SO) cutoff frequencies
	// kispi filter: use hard filter configuration. See filters.h
#define LowFcSO_KISPI				0.319825
#define HighFcSO_KISPI				3.12648

	// MHSL filter
#define SO_FcLow					0.2
#define SO_FcHigh					5
#define SO_Order					2
#define SO_CoeffOrder				(2*SO_Order)+1

// After detecting Sleep, wait until SWD filter is stable
#define SO_FILTERING_DELAY_IN_SEC	10
#define SO_FILTERING_DELAY			SO_FILTERING_DELAY_IN_SEC*SAMPLE_FREQ // SO filtering delay in samples

//----------------------------------------
// * SWD_METHOD = 1;
// * Power condition Slow Wave detection
// * SWD_METHOD	must be 0 to enable this method
#define SWD_PWR_PowWind 			1   // SAMPLE_FREQmean square in SWD_PWR_PowWind window. Value in samples.
#define SWD_PWR_mvaLength 			(4 * SAMPLE_FREQ/SWD_PWR_PowWind)  // times length of window: SWD_PWR_mvaLength times the SWD_PWR_PowWind
#define SWD_PWR_filtOrder 			2
#define SWD_PWR_deltaFiltLow 		0.5
#define SWD_PWR_deltaFiltHi 		4



/******************************/
/* STIMULATION METHOD	      */
/******************************/

#define STIM_METHOD	       0 // STIM_METHOD=0: PLL phase detection method; STIM_METHOD=1: fixed inter tone interval.

//----------------------------------------
// * STIM_METHOD = 1;
// * Fixed inter tone interval. After the
// * detection of the first tone with the PLL
#define INTER_TONE_INTERVAL	 250 //269  // This value is in samples. Then min ITI = 4ms if 1 is defined




/******************************/
/* Seep detection by Caroline */
/******************************/
// Frequency bands: only if second order filter is used
#define LowFcDelta2			2	// Low cut-off frequency for Delta2
#define HighFcDelta2		4	// High cut-off frequency for Delta2
#define LowFcDelta4			3	// Low cut-off frequency for Delta4
#define HighFcDelta4		5	// High cut-off frequency for Delta4
#define LowFcMuscPow2		20	// Low cut-off frequency for Muscle
#define HighFcMuscPow2		30 	// High cut-off frequency for Muscle


//Configuration for power window size and average window size
#define POW_CALC_WIND_SEC 	4
#define POW_CALC_WIND     	(POW_CALC_WIND_SEC*SAMPLE_FREQ)
#define AVG_CALC_WIND_SEC 	80
#define AVG_CALC_SAMP  		(AVG_CALC_WIND_SEC/POW_CALC_WIND_SEC)
#define AVG_GAIN			4

// Young adults thresholds
#define Y_D4_OVER_M_TH   16
#define Y_D2_TH   700
#define Y_Muscle_TH   40


/******************************/
/*   HARD CONDITIONS          */
/******************************/
#define ENABLE_HARD_COND_5	0	// alpha+beta condition

// Condition 0: Initital onset time. This condition can be disable by making time = 0;
#define INITIAL_SLEEP_ONSET_TIME 		10*60 // Initial onset time in seconds
#define INITIAL_SLEEP_ONSET_TIME_SAMP  	INITIAL_SLEEP_ONSET_TIME*SAMPLE_FREQ*ENABLE_HARD_COND_0 // Initial onset time on samples

// Condition 1: Sleep Onset after NREM to REM/Awake transition. This condition can be disable by making MAX_REM_AWK_TIME = 0;
#define SLEEP_ONSET_TIME 		2*60 // Onset time in seconds
#define MAX_REM_AWK_TIME 		3*60 // Maximum REM or AWAKE time (in seconds) for non considering sleep onset time
#define SLEEP_ONSET_TIME_SAMP  	SLEEP_ONSET_TIME*SAMPLE_FREQ*ENABLE_HARD_COND_1 // onset time on samples
#define MAX_REM_AWK_TIME_SAMP	MAX_REM_AWK_TIME*SAMPLE_FREQ*ENABLE_HARD_COND_1 // Maximum REM or AWAKE time in samples

// condition 2: delay between clicks
// Basic condition of no more than one tone per slow wave
#define MIN_TIME_BETW_TONE_SEC			0.5  	// minimum time between tones to be considered as different slow wave's tones
#define MIN_TIME_BETW_TONE_SAMP  		MIN_TIME_BETW_TONE_SEC*SAMPLE_FREQ  // in samples

// On-Off windows time
#define STIM_ON_TIME_WIND				6*SAMPLE_FREQ	//TIME_IN_SEC*SAMPLE_FREQ
#define STIM_OFF_TIME_WIND				6*SAMPLE_FREQ	//TIME_IN_SEC*SAMPLE_FREQ


// condition 3: Movement detection -> 300 < peak Amplitude < -300 for example, is considered as a movement detection and a break time is applied
#define SW_DOWN_THRESHOLD 		   -600 	// If wave is smaller than this threshold, then it is considered a movement
#define SW_UP_THRESHOLD 			600 	// If wave is bigger than this threshold, then it is considered a movement
#define MOV_DET_BREAK_SEC			10		// break time in sec after movement detection
#define MOV_DET_BREAK_SAMP			MOV_DET_BREAK_SEC*SAMPLE_FREQ


// condition 4: 4s beta increase eg over 20 while slow waves are stimulated to stop for about 10s but only while stimulations/triggers happen. This condition only starts one all 3 other conditions are met to track arousals due to stimulation
#define betaTH_PG             5  // Proportional gain of the calculated beta threshold
#define LowFcBeta             17 // Low cut-off frequency for Beta
#define HighFcBeta            22 // High cut-off frequency for Beta

#define BETA_POW_CALC_WIND_SEC 		1 //configuration of windows for power calculation
#define BETA_POW_CALC_WIND     		(BETA_POW_CALC_WIND_SEC*SAMPLE_FREQ)

#define BETA_AVG_CALC_SAMP  		(INITIAL_SLEEP_ONSET_TIME/BETA_POW_CALC_WIND_SEC)
#define BETA_AVG_GAIN				1
#define HC4_ONSET_TIME				(10*SAMPLE_FREQ/BETA_POW_CALC_WIND)


// condition 5:
#define alphaTH_PG			        10
#define LowFcAlpha                  10
#define HighFcAlpha                 11

#define ALPHA_POW_CALC_WIND_SEC 	1 //configuration of windows for power calculation
#define ALPHA_POW_CALC_WIND     	(ALPHA_POW_CALC_WIND_SEC*SAMPLE_FREQ)

#define ALPHA_AVG_CALC_SAMP  		(INITIAL_SLEEP_ONSET_TIME/ALPHA_POW_CALC_WIND_SEC) // For the 10 min (sleep onset time) threshold calculation
#define ALPHA_AVG_GAIN				1
#define HC5_ONSET_TIME				(10*SAMPLE_FREQ/ALPHA_POW_CALC_WIND)

// condition 6: PLL condition to not stimulate - 1 raw amplitude < 0 and Negative amplitude
#define SAMPLES_PER_SLOPE			20
#define	HC6_THRESHOLD				10
/******************************/
/*         AUDIO              */
/******************************/
#define TONE_DURATION		0.05	// duration in seconds
#define INITIAL_VOLUME		38  // Equivalent to 50dB
#define VOLUME_STEP			1	// Equivalent to 3dB
#define MAXIMUM_VOLUME		44
#define MINIMUM_VOLUME		32

#define AROUSAL_TONE_TIME	4*SAMPLE_FREQ-2

#define AUDIO_BUFFER_SIZE   4096
#define TONE_TYPE			"PINK_NOISE"

#define TONE_DURATION_IN_SAMP		TONE_DURATION*SAMPLE_FREQ
#define VC_CONSECUTIVE_TONES		7
#define VC_CONSECUTIVE_AROUS		2
#define VC_TONES_BETWEEN_AROUSALS	2
#define VC_INCREASE_VOL_DELTA2_Y	400
#define VC_INCREASE_VOL_DELTA2_E	150

#endif /* CONFIG_H_ */




















































































































































































































































































































































































































































































































































































































































































































































/*
 * config.h
 *
 *  Created on: 23.06.2017
 *      Author: fersterm
 */

#ifndef CONFIG_H_
#define CONFIG_H_

/******************************/
/* AvatarEEG frame definition */
/******************************/
/*#define FRAME_HEADER     		20
#define FRAME_MAX_LENGTH 		FRAME_HEADER+(16*9*3)+2 // HEADER + PAYLOAD (16=samples, 9=  ,3= ) + CRC(2Bytes)
#define FRAME_MIN_LENGTH 		FRAME_HEADER
#define FRAME_STORE_DEEP 		10

#define FromSampleTo_uVolt 	750000/pow(2,24)         // converts the ADC sample to  microVolts values. Range=750mV. 24 bits => 750000[uV]/(2^24)

#define DOWN_SAMP_FACTOR		1 // can get values of: 1, 2, 4, 8 or 16
#define SAMPLES_PER_FRAME_DS    SAMPLES_PER_FRAME/DOWN_SAMP_FACTOR
*/
#define SAMPLES_PER_FRAME 	    16
#define SAMPLES_PER_FRAME_DS    16


/******************************/
/* General definition */
/******************************/
#define LOAD_DATA_FROM_KISPI 	1
#define SAMPLE_FREQ   			(250*(1+LOAD_DATA_FROM_KISPI))



/******************************/
/*     Phase-locked loop      */
/******************************/
#define GAIN_PD 		3
#define GAIN_NCO		6
#define TARGET_PHASE 	M_PI/4  // Hz
#define CENTRAL_FREQ 	1		// Central NCO frequency at 1Hz
#define Wc 				(2*M_PI*CENTRAL_FREQ/SAMPLE_FREQ)
#define HIGHPASS_FREQ	0.1


/******************************/
/* SLOW WAVE DETECTOR         */
/******************************/
#define SW_THRESHOLD 				-40
#define SW_UP_THRESHOLD 			-300				// If wave is smaller than this threshold, then it is considered an arausal
#define FREQ_CONDITION_LOW_LIM  	0.5*SAMPLE_FREQ/2 	// Half period of a 0.5Hz wave
#define FREQ_CONDITION_HIGH_LIM 	2*SAMPLE_FREQ/2 	// Half period of a 2Hz wave

#define SWD_WITH_UP_TH				1
#define SWD_FREQ_UP_COND			1

//#define SWD_WITH_UP_TH

// Slow Oscilation (SO) cutoff frequencies
#define LowFcSO						0.2
#define HighFcSO					5

// After detecting Sleep, wait until NREM sleep is stable
#define SW_DETECTION_DELAY_IN_SEC	(60*3-10)
#define SW_DETECTION_DELAY			SW_DETECTION_DELAY_IN_SEC*SAMPLE_FREQ
#define SO_FILTERING_DELAY			10*SAMPLE_FREQ

/******************************/
/*Seep detection by Caroline*/
/******************************/
// Frequency bands
#define LowFcDelta2			2
#define HighFcDelta2		4
#define LowFcDelta4			3
#define HighFcDelta4		5
#define LowFcBeta2			19
#define HighFcBeta2			38
#define LowFcMuscPow2		20
#define HighFcMuscPow2		30

// Power and average windows
//config for 20 sec epoch
/*#define POW_CALC_WIND_SEC 	4
#define POW_CALC_WIND     	(POW_CALC_WIND_SEC*SAMPLE_FREQ)
#define AVG_CALC_WIND_SEC 	20
#define AVG_CALC_SAMP  		(AVG_CALC_WIND_SEC/POW_CALC_WIND_SEC)
#define AVG_GAIN			1*/

//config for 80 sec epoch
#define POW_CALC_WIND_SEC 	4
#define POW_CALC_WIND     	(POW_CALC_WIND_SEC*SAMPLE_FREQ)
#define AVG_CALC_WIND_SEC 	80
#define AVG_CALC_SAMP  		(AVG_CALC_WIND_SEC/POW_CALC_WIND_SEC)
#define AVG_GAIN			4




// Thresholds acording to population: "Y": for young adults. "E": for elderly
#define POPULATION 			"Y"

	// Elderly
#define E_D4_OVER_B2_TH 	4.5
#define E_B2_TH				24
#define E_Muscle_TH			16

	// Young adults
#define Y_D4_OVER_B2_TH 	5
#define Y_D2_TH				176
#define Y_Muscle_TH			20


/******************************/
/*Seep detection by Santostasi*/
/******************************/

#define DELTA_RMS40_TH 			20.35868845
#define ALPHA_RMS70_TH 			6.9069
#define BETA_RMS70_TH  			2.873

#define RMS_WINDOW_SIZE 	  	SAMPLE_FREQ*60*5 				     // 5 minutes of for rms calculation
#define RMS_WINDOW_STEP 	  	31 //(int)(SAMPLE_FREQ/SAMPLES_PER_FRAME)	 // 1 second of rms window step
#define RMS_WINDOW_SHIFT_IDX  	RMS_WINDOW_SIZE-SAMPLES_PER_FRAME

// SW detection condition
#define LOW_FREQ_COND 			SAMPLE_FREQ/2
#define HIG_FREQ_COND 			SAMPLE_FREQ*2
#define AMP_THRESHOLD			-50
#define MIN_NUM_OF_SW			6
#define SWD_EPOCH		    	SAMPLE_FREQ*30 		// 30 seconds epoch for Slow Wave detetection (SWD) criteria
#define START_ptr 				RMS_WINDOW_SIZE - SWD_EPOCH - 1


// AROUSAL DETECTION
#define REFACTORY_TIME_IN_FRAME_NUMBER 31 //930 //(int)(SAMPLE_FREQ*30/SAMPLES_PER_FRAME) 		// 30 seconds of refactory time

#define STOP_TIME 	31 //(int)(SAMPLE_FREQ/SAMPLES_PER_FRAME)


/******************************/
/*         AUDIO              */
/******************************/

#define TONE_DURATION		0.05  // 50 ms duration
#define AUDIO_FREQUENCY		16000 // 16 KHZ
#define AUDIO_VOLUME		35
#define AUDIO_BUFFER_SIZE   4096
#define TONE_TYPE			"PINK_NOISE"

#define TONE_DURATION_IN_SAMP	TONE_DURATION*AUDIO_FREQUENCY


#endif /* CONFIG_H_ */

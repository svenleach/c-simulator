/*
 * ==========================================================================================================
 * Name        : filters.c
 * Author      : Maria Laura Ferster
 * Version     : 1.0
 * Date		   : 23-June-2017
 * Copyright   : (c) 2017.  Maria Laura Ferster, Mobile Health Systems Lab, ETH Zurich. All rights reserved
 *
 * Description : Set of filters used for EEG-preprocessing
 *
 * Objective   : Test different filters set under float point precision to simulate hardware
 ============================================================================================================
 */

#include "filters.h"
#include "iir.h"


float NotchFilter_50Hz(float EEG_RawData){
	/************************************************************
	 * ** Notch filter @ 50Hz **
	 * @brief: Eliminate the 50Hz noise introduced by the power line
	 * 				 Filter type: iir
	 * 				 Filter order = 1
	 * 				 coefficients calculation on Matlab
	 * 						wo = 50/(fs/2);  bw = wo/NF_fact;
	 * 						[b,a] = iirnotch(wo,bw);
	 *************************************************************/

	// Notch filter initialization
	int NF_Csize  = 3; //(2*NF_order)+1
	float Num[3] = {0.9598, -0.5932, 0.9598}; //NF_fact = 15
	float Den[3] = {1.0000, -0.5932, 0.9195}; //NF_fact = 15
	static float raw_input_buff[3];
	static float filterOut_buff[3];

	for (int i = 1; i < NF_Csize; i++){
		raw_input_buff[NF_Csize-i] = raw_input_buff[NF_Csize-1-i]; // So loop with 5 steps is done in 1 period (1 sample!)
		filterOut_buff[NF_Csize-i] = filterOut_buff[NF_Csize-1-i]; // So loop with 5 steps is done in 1 period (1 sample!)
	}
	raw_input_buff[0] = EEG_RawData;
	filterOut_buff[0] = 0;

	filterOut_buff[0] = ButterN_Order(raw_input_buff, filterOut_buff, Num, Den, 1);
	EEG_RawData = filterOut_buff[0];

	return EEG_RawData;

}


float HighPassFilterT_01Hz_HC3(float data){

    float RC = 1.0/(HIGHPASS_FREQ*(float)M_2PI);
    float dt = 1.0/SAMPLE_FREQ;
    float alpha = RC/(RC + dt);
    static float filteredData;
    static float prev_data = 0.0;

    filteredData = alpha * (filteredData + data - prev_data);
    prev_data = data;

    return filteredData;
}

// ****************** BEGINING OF BUTTERWORTH 2nd Order FILTER **********************
/**
 * @brief  Numerator and denominator coefficient generation functions for 2th order butterworth bandpass or lowpass filters
 * @param
 * @retval
 */
float Butter2ndOrder(float INPUTS[], float filterOut[], float Ncoeff[], float Dcoeff[]){

	//4 past inputs
	for (int i = 0; i < 5; i++)
		filterOut[0] += Ncoeff[i]*INPUTS[i];

	//4 past outputs
	for (int i = 1; i < 5; i++)
		filterOut[0] -= Dcoeff[i]*filterOut[i];

	// return current output
	return filterOut[0];
}


float* ButterNum (float fc[], float fs){
	float *N;
	N = malloc(sizeof(float) * 5);
	if (fc[0]!=0){
		float fcW[] = {4*(tan((float)M_PI*fc[0]/(fs/2)/2)),4*(tan((float)M_PI*fc[1]/(fs/2)/2))};
		float w0 = sqrt(fcW[0]*fcW[1]);
		float dW = fcW[1]-fcW[0];
		float leadCoef = pow(w0,4)+4*sqrt(2)*pow(w0,2)*dW+32*pow(w0,2)+16*pow(dW,2)+64*sqrt(2)*dW+256;
		float numCoef = 16*pow(dW,2)/leadCoef;
		N[0] = numCoef;
		N[1] = 0;
		N[2] = -2*numCoef;
		N[3] = 0;
		N[4] = numCoef;

	}else{//if fc[0]==0, then 2nd order lowpass is calculated
		float w0 = 4*(tan((float)M_PI*fc[1]/(fs/2)/2));
		float leadCoef = pow(w0,4)+4*sqrt(4+sqrt(8))*pow(w0,3)+(16*sqrt(2)+32)*pow(w0,2)+64*sqrt(4+sqrt(8))*w0+256;
		float numCoef = pow(w0,4)/leadCoef;
		N[0] = numCoef;
		N[1] = 4*numCoef;
		N[2] = 6*numCoef;
		N[3] = 4*numCoef;
		N[4] = numCoef;
	}
	return N;
}

float* ButterDen (float fc[],float fs){
	float *D;
	D = malloc(sizeof(float) * 5);
	if (fc[0]!=0){
		float fcW[] = {4*(tan((float)M_PI*fc[0]/(fs/2)/2)),4*(tan((float)M_PI*fc[1]/(fs/2)/2))};
		float w0 = sqrt(fcW[0]*fcW[1]);
		float dW = fcW[1]-fcW[0];
		float leadCoef = pow(w0,4)+4*sqrt(2)*pow(w0,2)*dW+32*pow(w0,2)+16*pow(dW,2)+64*sqrt(2)*dW+256;
		D[0] = 1;
		D[1] = (4*pow(w0,4)+8*sqrt(2)*pow(w0,2)*dW-128*sqrt(2)*dW-1024)/leadCoef;
		D[2] = (6*pow(w0,4)-64*pow(w0,2)-32*pow(dW,2)+1536)/leadCoef;
		D[3] = (4*pow(w0,4)-8*sqrt(2)*pow(w0,2)*dW+128*sqrt(2)*dW-1024)/leadCoef;
		D[4] = (pow(w0,4)-4*sqrt(2)*pow(w0,2)*dW+32*pow(w0,2)+16*pow(dW,2)-64*sqrt(2)*dW+256)/leadCoef;
	}else{
		float w0 = 4*(tan((float)M_PI*fc[1]/(fs/2)/2));
		float leadCoef = pow(w0,4)+4*sqrt(4+sqrt(8))*pow(w0,3)+(16*sqrt(2)+32)*pow(w0,2)+64*sqrt(4+sqrt(8))*w0+256;
		//float numCoef = pow(w0,4)/leadCoef;
		D[0] = 1;
		D[1] = (4*pow(w0,4)+8*sqrt(4+sqrt(8))*pow(w0,3)-128*sqrt(4+sqrt(8))*w0-1024)/leadCoef;
		D[2] = (6*pow(w0,4)-(32*sqrt(2)+64)*pow(w0,2)+1536)/leadCoef;
		D[3] = (4*pow(w0,4)-8*sqrt(4+sqrt(8))*pow(w0,3)+128*sqrt(4+sqrt(8))*w0-1024)/leadCoef;
		D[4] = (pow(w0,4)-4*sqrt(4+sqrt(8))*pow(w0,3)+(16*sqrt(2)+32)*pow(w0,2)-64*sqrt(4+sqrt(8))*w0+256)/leadCoef;
	}
	return D;
}


float* CoeffCalculation (float LowFreq, float HighFreq){

	float fc[2];
	float Fs = SAMPLE_FREQ;
	float *Coeffs;
	Coeffs = malloc(sizeof(float)*10);
	float *Ncoeff;
	float *Dcoeff;

	fc[0] = LowFreq;//, HighFcDelta2};
	fc[1] = HighFreq;
	Ncoeff = ButterNum(fc, Fs);
	Dcoeff = ButterDen(fc, Fs);

	for (int i = 0; i < 5; i++){
		Coeffs[i]   = Ncoeff[i];
		Coeffs[i+5] = Dcoeff[i];
	}
	return Coeffs;
}

// ****************** END OF BUTTERWORTH 2nd Order FILTER **********************

// ***************** BEGINING OF HARD FILTERS CONFIGURATION **************************


/**
 * @brief  IIR Filter - DIRECT-FORM II
 * @param  data
 * 		   N_SECTIONS
 * 		   BUFFER
 * 		   FILT_BUFF_SIZE
 * 		   COEFF
 * 		   OUT_GAIN
 * 		   SEC_GAIN
 * @retval
 */

float DirectFormII(float data, int N_SECTIONS, float *BUFFER, int FILT_BUFF_SIZE, float *COEFF, float OUT_GAIN, float *SEC_GAIN) {

	float FILTERED_data;
	uint8_t nb, nc, i; 	// Buffer and coefficient indexes: nb and nc;

	for (int secIdx = 0; secIdx < N_SECTIONS; secIdx++) {

		nb = secIdx * 3;
		nc = secIdx * 6;

		BUFFER[nb + 2] = SEC_GAIN[secIdx]*data - COEFF[nc + 4] * BUFFER[nb + 1] - COEFF[nc + 5] * BUFFER[nb];
		data = -BUFFER[nb + 2] * COEFF[nc + 2] - BUFFER[nb] * COEFF[nc] - BUFFER[nb + 1] * COEFF[nc + 1]; //Output section k

		for (i = 0; i < 3; i++) // buffer shifting
			BUFFER[nb+i] = BUFFER[nb+i + 1];
	}

	FILTERED_data = data * OUT_GAIN;

	return FILTERED_data;
}


/**
 * @brief  Slow Oscillations ([0.5-2]Hz) band filter:
 *					Respone: Band Pass filter
 *					Butterworth
 *					# of sections 	  : 3
 *					First 3-dB Point  : 319.825 mHz
 *					Second 3-dB Point : 3.1265 Hz
 * @param  EEG_RawData: data to be filtered
 * @retval EEG_FILTERED_DIR  : memory allocation of the filtered data
 */
float SO_KISPI_filter(float EEG_RawData, bool INIT_COND) {

	static float BUFFER_SO_KISPI[9] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};  // static variable: keeps the value among calls
	if (INIT_COND==1){
		for (int i=0; i<9; i++)
			BUFFER_SO_KISPI[i] = 0.0;
	}

	int N_SECTIONS = 3;
	int FILT_BUFF_SIZE = N_SECTIONS * 3;
	float OUT_GAIN = 1;
	float SEC_GAIN[3] = {0.034656785900228457, (float)0.034656785900228457, (float)0.03408156134090521};

	float COEFF[18] = { 1.0, 0.0, -1.0, 1.0, (float)-1.9329293199346691, (float)0.9384750679187136,
			 1.0, 0.0, -1.0, 1.0, (float)-1.992944408306514, (float)0.99301385770580441,
			 1.0, 0.0, -1.0, 1.0, (float)-1.9312265743122894,  (float)0.93183687731818965};


	float EEG_FILTERED_DIR; // Address of Stack memoty associated with EEG data filtered

	EEG_FILTERED_DIR = DirectFormII(EEG_RawData, N_SECTIONS, BUFFER_SO_KISPI,
			FILT_BUFF_SIZE, COEFF, OUT_GAIN, SEC_GAIN);

	return EEG_FILTERED_DIR;
}

float SO_MHSL_filter(float EEG_RawData) {

	int N_SECTIONS = 2;
	int FILT_BUFF_SIZE = N_SECTIONS * 3;
	float OUT_GAIN = 1;
	float SEC_GAIN[2] = {(float)0.057870004738908423, (float)0.057870004738908423};

	float COEFF[12] = { 1.0, 0.0, -1.0, 1.0, (float)-1.9929387010373354, (float)0.99296586526808039,
			 1.0, 0.0, -1.0, 1.0, (float)-1.8356084821916547, (float)0.84912675261567205};

	static float BUFFER_SO_KISPI[6]; // static variable: keeps the value among calls
	float EEG_FILTERED_DIR; // Address of Stack memoty associated with EEG data filtered

	EEG_FILTERED_DIR = DirectFormII(EEG_RawData, N_SECTIONS, BUFFER_SO_KISPI,
			FILT_BUFF_SIZE, COEFF, OUT_GAIN, SEC_GAIN);

	return EEG_FILTERED_DIR;
}

/**
 * @brief  Muscle ([20-30]Hz) band filter:
 *					Respone: Band Pass filter
 *					Butterworth
 *					# of sections 	  : 6
 *					First 3-dB Point  : 20 Hz
 *					Second 3-dB Point : 30 Hz
 * @param  EEG_RawData: data to be filtered
 * @retval EEG_FILTERED_DIR: value of the filtered data
 * @COEFF: Each section has 6 coefficients: 3 for numerator and 3 for denominator
 * therefore the coefficient vector size should be: # of sections * 6. Each section has
 * own gain defined as SEC_GAIN	and an output gain defined as OUT_GAIN
 */

float Muscle_6thOrder_filter(float EEG_RawData) {

	int N_SECTIONS = 6;
	int FILT_BUFF_SIZE = N_SECTIONS * 3;
	float OUT_GAIN = 1;
	float SEC_GAIN[6] = {(float)0.12148448401700658, (float)0.12148448401700658, (float)0.11558200564039579,
			(float)0.11558200564039579, (float)0.11254276534856621, (float)0.11254276534856621};

	float COEFF[36] = { 1.0, 0.0, -1.0, 1.0, (float)-1.4112986636945517, (float)0.927446781191827,
			 1.0, 0.0, -1.0, 1.0, (float)-1.7037249458796708, (float)0.94782090627240689,
			 1.0, 0.0, -1.0, 1.0, (float)-1.3754313419006134, (float)0.81859232320210618,
			 1.0, 0.0, -1.0, 1.0, (float)-1.5988790952737737, (float)0.85622203057889534,
			 1.0, 0.0, -1.0, 1.0, (float)-1.4942474830484551, (float)0.79186590139253055,
			 1.0, 0.0, -1.0, 1.0, (float)-1.4103413028034182, (float)0.77364404270391085};

	static float BUFFER_SO_KISPI[18]; // static variable: keeps the value among calls
	float EEG_FILTERED_DIR; // Address of Stack memory associated with EEG data filtered

	EEG_FILTERED_DIR = DirectFormII(EEG_RawData, N_SECTIONS, BUFFER_SO_KISPI,
			FILT_BUFF_SIZE, COEFF, OUT_GAIN, SEC_GAIN);

	return EEG_FILTERED_DIR;

}
//**************** END OF HARD FILTERS CONFIGURATION **********************************************


//**************** BEGINING OF GENERAL FILTERS CONFIGURATION **************************************

// Numerator and denominator filter coefficients calculation
float* FLT_CoeffCalc(float FreqCutL, float FreqCutH, uint8_t n){

	volatile float wc1;
	volatile float wc2;
	uint8_t i;
	float *Coeffs;
	int CoeffLen = (n*2)+1;
	Coeffs = malloc(sizeof(float)*CoeffLen*2);

	wc1 = FreqCutL  * 2;
	wc1 = wc1 / SAMPLE_FREQ;

	wc2 = FreqCutH  * 2;
	wc2 = wc2 / SAMPLE_FREQ;

	float_dcof_bwbp(n,wc1,wc2);

	float_ccof_bwbp(n);

	float_sf_bwbp(n,wc1,wc2);

	for (i=0; i < CoeffLen; i++)
	{
		Coeffs[i] 		  = (float)(ccof_bwbp[i]*sf_bwbp);  // Numerator coefficients
		Coeffs[CoeffLen+i] = (float)dcof_bwbp[i];			// Denominator coefficients
	}

	return Coeffs;
}


float ButterN_Order(float INPUTS[], float filterOut[], float Ncoeff[], float Dcoeff[], int Order){

	int CoeffLen = (Order*2)+1;
	//CoeffLen past inputs
	for (int i = 0; i < CoeffLen; i++){
		filterOut[0] = filterOut[0] + Ncoeff[i]*INPUTS[i];
	}
	//CoeffLen past outputs
	for (int i = 1; i < CoeffLen; i++){
		filterOut[0] = filterOut[0] - Dcoeff[i]*filterOut[i];
	}
	// return current output
	return filterOut[0];
}

//**************** BEGINING OF GENERAL FILTERS CONFIGURATION **************************************

/*float* HighPassFilter(float* data_v){

	float BUFFER[3];
	float data;

	for (int i = 0; i < 3; i++)
		BUFFER[i] = data_v[i];
	data = data_v[3];

	float data_filtered;
	float COEFF[6] = {1.0, -2.0, 1.0, 1.0, -1.997944427802433, 0.99794710515707707};

	float Gain = 0.99897288323987754;
	float Out_Gain = 0.97723722095581067;

	BUFFER[2] = data*Gain - COEFF[4] * BUFFER[1] - COEFF[5] * BUFFER[0];
	data_filtered = BUFFER[2]*COEFF[2] + BUFFER[0]*COEFF[0] + BUFFER[1]*COEFF[1]; //Output section k

    // buffer shifting
	for (int i = 0; i < 3; i++){
		BUFFER[i] = BUFFER[i + 1];
		//BuffAndFilteredData[i] = BUFFER[i];
	}
	for (int i = 0; i < 3; i++)
		data_v[i] = BUFFER[i];
	data_v[3] = data_filtered*Out_Gain;


    return data_v;
}

float HighPassFilter_01Hz(float data){

	static float BUFFER[3];
	float data_filtered;

	static float COEFF[6] = {1.0, -2.0, 1.0, 1.0, -1.997944427802433, 0.99794710515707707};

	static float Gain = 0.99897288323987754;
	static float Out_Gain = 0.97723722095581067;

	BUFFER[2] = data*Gain - COEFF[4] * BUFFER[1] - COEFF[5] * BUFFER[0];
	data_filtered = BUFFER[2]*COEFF[2] + BUFFER[0]*COEFF[0] + BUFFER[1]*COEFF[1]; //Output section k

    // buffer shifting
	for (int i = 0; i < 3; i++){
		BUFFER[i] = BUFFER[i + 1];
	}

    return data_filtered*Out_Gain;
}
*/

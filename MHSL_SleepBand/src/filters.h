/*
 * ==========================================================================================================
 * Name        : filters.h
 * Author      : Maria Laura Ferster
 * Version     : 1.0
 * Date		   : 12-June-2017
 * Copyright   : (c) 2017.  Maria Laura Ferster, Mobile Health Systems Lab, ETH Zurich. All rights reserved
 ============================================================================================================
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef FILTERS_H 
#define FILTERS_H

//#include "utils.h"
#include "config.h"
#include "main.h"
#include "stdint.h"


float NotchFilter_50Hz(float EEG_RawData);
float HighPassFilterT_01Hz_HC3(float data);

//--- Butterworth 2nd order Flter ------
float* ButterNum(float fc[], float fs);
float* ButterDen(float fc[], float fs);
float  Butter2ndOrder(float input[], float filterOut[], float Ncoeff[], float Dcoeff[]);
float* CoeffCalculation (float LowFreq, float HighFreq);

//---- HighPass filter for PLL raw input data -----
/*float* HighPassFilter(float* data);
float EEG_preprocessing(float PLL_input_raw);
float HighPassFilter_01Hz(float data);*/

//----- IIR filter Direct Form II ----------------
float DirectFormII(float data, int N_SECTIONS, float *BUFFER, int FILT_BUFF_SIZE, float *COEFF, float OUT_GAIN, float *SEC_GAIN);
float SO_KISPI_filter(float EEG_RawData, bool INIT_COND);
float SO_MHSL_filter(float EEG_RawData);
float Muscle_6thOrder_filter(float EEG_RawData);

//int delete_vectors();
float* FLT_CoeffCalc(float FreqCutL, float FreqCutH, uint8_t n);
float ButterN_Order(float INPUTS[], float filterOut[], float Ncoeff[], float Dcoeff[], int Order);
#endif //!defined

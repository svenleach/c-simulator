/*/*
 *                            COPYRIGHT
 *
 *  liir - Recursive digital filter functions
 *  Copyright (C) 2007 Exstrom Laboratories LLC
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  A copy of the GNU General Public License is available on the internet at:
 *
 *  http://www.gnu.org/copyleft/gpl.html
 *
 *  or you can write to:
 *
 *  The Free Software Foundation, Inc.
 *  675 Mass Ave
 *  Cambridge, MA 02139, USA
 *
 *  You can contact Exstrom Laboratories LLC via Email at:
 *
 *  stefan(AT)exstrom.com
 *
 *  or you can write to:
 *
 *  Exstrom Laboratories LLC
 *  P.O. Box 7651
 *  Longmont, CO 80501, USA
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "iir.h"
#include "utils.h"
#include "config.h"
/**********************************************************************
  trinomial_mult - multiplies a series of trinomials together and returns
  the coefficients of the resulting polynomial.

  The multiplication has the following form:

  (x^2 + b[0]x + c[0])*(x^2 + b[1]x + c[1])*...*(x^2 + b[n-1]x + c[n-1])

  The b[i] and c[i] coefficients are assumed to be complex and are passed
  to the function as a pointers to arrays of floats of length 2n. The real
  part of the coefficients are stored in the even numbered elements of the
  array and the imaginary parts are stored in the odd numbered elements.

  The resulting polynomial has the following form:

  x^2n + a[0]*x^2n-1 + a[1]*x^2n-2 + ... +a[2n-2]*x + a[2n-1]

  The a[i] coefficients can in general be complex but should in most cases
  turn out to be real. The a[i] coefficients are returned by the function as
  a pointer to an array of floats of length 4n. The real and imaginary
  parts are stored, respectively, in the even and odd elements of the array.
  Storage for the array is allocated by the function and should be freed by
  the calling program when no longer needed.

  Function arguments:

  n  -  The number of trinomials to multiply
  b  -  Pointer to an array of floats of length 2n.
  c  -  Pointer to an array of floats of length 2n.
*/
float trinomial[4*6];
void float_trinomial_mult( int n, float *b, float *c )
{
    int i, j;

		for (i=0;i<4*6;i++)
			trinomial[i] = 0.0;

    trinomial[2] = c[0];
    trinomial[3] = c[1];
    trinomial[0] = b[0];
    trinomial[1] = b[1];

    for( i = 1; i < n; ++i )
    {
			trinomial[2*(2*i+1)]   += c[2*i]*trinomial[2*(2*i-1)]   - c[2*i+1]*trinomial[2*(2*i-1)+1];
			trinomial[2*(2*i+1)+1] += c[2*i]*trinomial[2*(2*i-1)+1] + c[2*i+1]*trinomial[2*(2*i-1)];
			for( j = 2*i; j > 1; --j )
			{
				trinomial[2*j]   += b[2*i] * trinomial[2*(j-1)]   - b[2*i+1] * trinomial[2*(j-1)+1] +
														c[2*i] * trinomial[2*(j-2)]   - c[2*i+1] * trinomial[2*(j-2)+1];
				trinomial[2*j+1] += b[2*i] * trinomial[2*(j-1)+1] + b[2*i+1] * trinomial[2*(j-1)] +
														c[2*i] * trinomial[2*(j-2)+1] + c[2*i+1] * trinomial[2*(j-2)];
			}

			trinomial[2] += b[2*i] * trinomial[0] - b[2*i+1] * trinomial[1] + c[2*i];
			trinomial[3] += b[2*i] * trinomial[1] + b[2*i+1] * trinomial[0] + c[2*i+1];
			trinomial[0] += b[2*i];
			trinomial[1] += b[2*i+1];
    }
}


/**********************************************************************
  dcof_bwbp - calculates the d coefficients for a butterworth bandpass
  filter. The coefficients are returned as an array of floats.
*/

float dcof_bwbp[(2*6)+1];

void float_dcof_bwbp( int n, float f1f, float f2f )
{
    int k;            // loop variables
    float theta;     // M_PI * (f2f - f1f) / 2.0
    float cp;        // cosine of phi
    float st;        // sine of theta
    float ct;        // cosine of theta
    float s2t;       // sine of 2*theta
    float c2t;       // cosine 0f 2*theta
    float parg;      // pole angle
    float sparg;     // sine of pole angle
    float cparg;     // cosine of pole angle
    float a;         // workspace variables
		float rcof[2 * n];
    float tcof[2 * n];

		for (k=0; k<(2*n)+1;k++)
			dcof_bwbp[k] = 0.0;

    cp    = cosf((float)M_PI * (f2f + f1f) / (float)2.0);
    theta = (float)M_PI * (f2f - f1f) / (float)2.0;
    st    = sinf(theta);
    ct    = cosf(theta);
    s2t   = (float)2.0*st*ct;        // sine of 2*theta
    c2t   = (float)2.0*ct*ct - (float)1.0;  // cosine of 2*theta

    for( k = 0; k < n; ++k )
    {
		 	parg        = (float)M_PI * (float)(2*k+1)/(float)(2*n);
			sparg       = sinf(parg);
			cparg       = cosf(parg);
			a           = (float)1.0 + s2t*sparg;
			rcof[2*k]   = c2t/a;
			rcof[2*k+1] = s2t*cparg/a;
			tcof[2*k]   = -2.0*cp*(ct+st*sparg)/a;
			tcof[2*k+1] = -2.0*cp*st*cparg/a;
    }

    float_trinomial_mult( n, tcof, rcof );

    dcof_bwbp[1] = trinomial[0];
    dcof_bwbp[0] = 1.0;
		dcof_bwbp[2] = trinomial[2];
    for( k = 3; k <= 2*n; ++k )
        dcof_bwbp[k] = trinomial[2*k-2];
}



/**********************************************************************
  ccof_bwlp - calculates the c coefficients for a butterworth lowpass
  filter. The coefficients are returned as an array of integers.
*/

int ccof_bwlp[6+1];

void float_ccof_bwlp( int n )
{
    int m;
    int i;

		for (i=0; i < n+1; i++) /* Clean buffer */
			ccof_bwlp[i] = 0;

    ccof_bwlp[0] = 1;
    ccof_bwlp[1] = n;
    m = n/2;
    for( i=2; i <= m; ++i)
    {
        ccof_bwlp[i]  = (n-i+1)*ccof_bwlp[i-1]/i;
        ccof_bwlp[n-i]= ccof_bwlp[i];
    }
    ccof_bwlp[n-1] = n;
    ccof_bwlp[n] = 1;
}


/**********************************************************************
  ccof_bwhp - calculates the c coefficients for a butterworth highpass
  filter. The coefficients are returned as an array of integers.

*/

int	ccof_bwhp[6];

void float_ccof_bwhp( int n )
{
    int i;

		for (i=0; i<n;i++)
			ccof_bwhp[i] = 0;

		float_ccof_bwlp( n );

    for( i = 0; i <= n; ++i)
        if( i % 2 )
					ccof_bwhp[i] = -ccof_bwlp[i];
				else
					ccof_bwhp[i] = ccof_bwlp[i];
}


/**********************************************************************
  ccof_bwbp - calculates the c coefficients for a butterworth bandpass
  filter. The coefficients are returned as an array of integers.

*/

int ccof_bwbp[2*6+1];

void float_ccof_bwbp( int n )
{
    int i;

    float_ccof_bwhp(n);

    for( i = 0; i < n; ++i)
    {
        ccof_bwbp[2*i] = ccof_bwhp[i];
        ccof_bwbp[2*i+1] = 0.0;
    }
    ccof_bwbp[2*n] = ccof_bwhp[n];
}



/**********************************************************************
  sf_bwbp - calculates the scaling factor for a butterworth bandpass filter.
  The scaling factor is what the c coefficients must be multiplied by so
  that the filter response has a maximum value of 1.

*/

float sf_bwbp;

void float_sf_bwbp( int n, float f1f, float f2f )
{
    int k;            // loop variables
    float ctt;       // cotangent of theta
    float sfr, sfi;  // real and imaginary parts of the scaling factor
    float parg;      // pole angle
    float sparg;     // sine of pole angle
    float cparg;     // cosine of pole angle
    float a, b, c;   // workspace variables

    ctt = (float)1.0 / tanf((float)M_PI * (f2f - f1f) / (float)2.0);
    sfr = (float)1.0;
    sfi = (float)0.0;

    for( k = 0; k < n; ++k )
    {
			parg = (float)M_PI * (float)(2*k+1)/(float)(2*n);
			sparg = ctt + sin(parg);
			cparg = cosf(parg);
			a = (sfr + sfi)*(sparg - cparg);
			b = sfr * sparg;
			c = -sfi * cparg;
			sfr = b - c;
			sfi = a - b - c;
    }

    sf_bwbp = ( (float)1.0 / sfr );
}

/*
 * ==========================================================================================================
 * Name        : iir.h
 * Author      : Maria Laura Ferster
 * Version     : 1.0
 * Date		   : 23-June-2017
 * Copyright   : (c) 2017.  Maria Laura Ferster, Mobile Health Systems Lab, ETH Zurich. All rights reserved
 ============================================================================================================
 */

#ifndef _IIR_H
#define _IRR_H
//#ifndef M_PI
//#    define M_PI 3.14159265358979323846
//#endif

extern int ccof_bwbp[];
extern float dcof_bwbp[];
extern float sf_bwbp;
void float_trinomial_mult( int n, float *b, float *c );
void float_dcof_bwbp( int n, float f1f, float f2f );
void float_ccof_bwbp( int n );
void float_sf_bwbp( int n, float f1f, float f2f );
#endif /* IIR_H_ */

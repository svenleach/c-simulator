/*
 * ==========================================================================================================
 * Name        : main.c
 * Author      : Maria Laura Ferster
 * Version     : 1.0
 * Date		   : 06-June-2017
 * Copyright   : (c) 2017.  Maria Laura Ferster, Mobile Health Systems Lab, ETH Zurich. All rights reserved
 *
 * Description : Modular implementation of algorithms to be implemented in the microcontroller for sleep
 * 	enhancement using wearables for WESA study
 *
 * Objective: Hardware offline performance evaluation (float point precision)
 ============================================================================================================
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "main.h"
#include "filters.h"
#include "PLL.h"
#include "SW_detector.h"
#include "sleep_detector2.h"
#include "HardConditions.h"
#include "VolumeControl.h"

int dataLen = 0;

/*
 * Generate files in order to save information for post processing analysis. Select files in logger.h
 */

void open_save_files(){
 // Note Replace with #if define function
	if (SAVE_FILTERED_DATA_SD == 1) {

		f_FILTER_D2   = fopen(SD_FILTER_D2_DATA_FILE, "w");
		f_FILTER_D4   = fopen(SD_FILTER_D4_DATA_FILE, "w");
		f_FILTER_B2   = fopen(SD_FILTER_B2_DATA_FILE, "w");
		f_FILTER_MP   = fopen(SD_FILTER_MP_DATA_FILE, "w");


		if ( (f_FILTER_D2 == NULL) || (f_FILTER_D4 == NULL) || (f_FILTER_B2 == NULL) || (f_FILTER_MP == NULL) ) {
			printf("Error opening SD_FILTER_D2_DATA_FILE file!\n");
			exit(1);
		}
	}


	if (SAVE_SLEEP_DETECTION == 1) {

		f_SD = fopen(SD_DATA_FILE, "w");
		if (f_SD == NULL) {
			printf("Error opening @@#$ SD_DATA_FILE file!\n");
			exit(1);
		}
	}

	if (SAVE_SD_POW_D2 == 1){
		f_SD_POW_D2 = fopen(SD_POW_CALC_D2_FILE, "w");
		if (f_SD_POW_D2 == NULL) {
			printf("Error opening SD_POW_CALC_D2_FILE file!\n");
			exit(1);
		}
	}

	if (SAVE_SD_POW_D4 == 1){
			f_SD_POW_D4 = fopen(SD_POW_CALC_D4_FILE, "w");
			if (f_SD_POW_D4 == NULL) {
				printf("Error opening SD_POW_CALC_D4_FILE file!\n");
				exit(1);
			}
		}

	if (SAVE_SD_POW_B2 == 1){
			f_SD_POW_B2 = fopen(SD_POW_CALC_B2_FILE, "w");
			if (f_SD_POW_B2 == NULL) {
				printf("Error opening SD_POW_CALC_B2_FILE file!\n");
				exit(1);
			}
		}


	if (SAVE_SD_POW_MP == 1){
			f_SD_POW_MP = fopen(SD_POW_CALC_MP_FILE, "w");
			if (f_SD_POW_MP == NULL) {
				printf("Error opening SD_POW_CALC_MP_FILE file!\n");
				exit(1);
			}
		}

	if (SAVE_SW_DETECTION == 1) {

		f_SWD = fopen(SWD_DATA_FILE, "w");
		if (f_SWD == NULL) {
			printf("Error opening SWD_DATA_FILE file!\n");
			exit(1);
		}
	}
	if (SAVE_SW_TH == 1) {

		f_SW_TH = fopen(SW_TH_DATA_FILE, "w");
		if (f_SW_TH == NULL) {
			printf("Error opening SWD_DATA_FILE file!\n");
			exit(1);
		}
	}
	if (SAVE_SW_FILTERED_DATA == 1) {

		f_SW_FILT = fopen(SW_FILTER_DATA_FILE, "w");
		if (f_SW_FILT == NULL) {
			printf("Error opening SWD_DATA_FILE file!\n");
			exit(1);
		}
	}

	if (SAVE_PHASE_DETECTION == 1) {

		f_PD = fopen(PLL_PD_DATA_FILE, "w");
		if (f_PD == NULL) {
			printf("Error opening PLL_PD_DATA_FILE file!\n");
			exit(1);
		}
	}

	if (SAVE_PLL_ARGUMENT == 1) {

		f_PLL_ARG = fopen(PLL_ARG_DATA_FILE, "w");
			if (f_PLL_ARG == NULL) {
				printf("Error opening PLL_ARG_DATA_FILE file!\n");
				exit(1);
			}
	}

	if (SAVE_PLL_FILTERED_DATA == 1) {

		f_PLL_INP = fopen(PLL_FILT_DATA_FILE, "w");
			if (f_PLL_INP == NULL) {
				printf("Error opening PLL_INPUT_DATA_FILE file!\n");
				exit(1);
			}
	}


	if (SAVE_STIM_CONDITION == 1) {

		f_STIM_COND = fopen(STIM_COND_DATA_FILE, "w");
			if (f_STIM_COND == NULL) {
				printf("Error opening STIM_COND_DATA_FILE file!\n");
				exit(1);
			}
	}

	if (SAVE_HARD_COND_1 == 1) {

		f_HARD_COND1 = fopen(HARD_COND_1_FILE, "w");
			if (f_HARD_COND1 == NULL) {
				printf("Error opening HARD_COND_1_FILE file!\n");
				exit(1);
			}
	}

	if (SAVE_HARD_COND_2 == 1) {

		f_HARD_COND2 = fopen(HARD_COND_2_FILE, "w");
			if (f_HARD_COND2 == NULL) {
				printf("Error opening HARD_COND_1_FILE file!\n");
				exit(1);
			}
	}
	if (SAVE_HARD_COND_3 == 1) {

		f_HARD_COND3 = fopen(HARD_COND_3_FILE, "w");
			if (f_HARD_COND3 == NULL) {
				printf("Error opening HARD_COND_1_FILE file!\n");
				exit(1);
			}
	}

	if (SAVE_HARD_COND_4 == 1) {

		f_HARD_COND4 = fopen(HARD_COND_4_FILE, "w");
			if (f_HARD_COND4 == NULL) {
				printf("Error opening HARD_COND_4_FILE file!\n");
				exit(1);
			}
		f_HC4_AVG_TH = fopen(HC4_BETA_AVG_TH_FILE, "w");
			if (f_HC4_AVG_TH == NULL) {
				printf("Error opening HC4_BETA_AVG_TH_FILE file!\n");
				exit(1);
			}
	}


	if (SAVE_HC4_BETA_FILT_OUT == 1) {

		f_HC4_FILT_OUT = fopen(HC4_BETA_FILT_OUT_FILE, "w");
			if (f_HC4_FILT_OUT == NULL) {
				printf("Error opening HC4_BETA_FILT_OUT_FILE file!\n");
				exit(1);
			}
	}

	if (SAVE_HC4_BETA_AVG_POW == 1) {

		f_HC4_AVG_POW = fopen(HC4_BETA_AVG_POW_FILE, "w");
			if (f_HC4_AVG_POW == NULL) {
				printf("Error opening HC4_BETA_FILT_OUT_FILE file!\n");
				exit(1);
			}
	}

	if (SAVE_HARD_COND_5 == 1) {

			f_HARD_COND5 = fopen(HARD_COND_5_FILE, "w");
				if (f_HARD_COND5 == NULL) {
					printf("Error opening HARD_COND_5_FILE file!\n");
					exit(1);
				}
			f_HC5_AVG_TH = fopen(HC5_ALPHA_AVG_TH_FILE, "w");
				if (f_HC5_AVG_TH == NULL) {
					printf("Error opening HC5_ALPHA_AVG_TH_FILE file!\n");
					exit(1);
				}
		}
	if (SAVE_HARD_COND_6 == 1) {

			f_HARD_COND6 = fopen(HARD_COND_6_FILE, "w");
				if (f_HARD_COND6 == NULL) {
					printf("Error opening HARD_COND_6_FILE file!\n");
					exit(1);
				}

		}

		if (SAVE_HC5_ALPHA_FILT_OUT == 1) {

			f_HC5_FILT_OUT = fopen(HC5_ALPHA_FILT_OUT_FILE, "w");
				if (f_HC5_FILT_OUT == NULL) {
					printf("Error opening HC5_ALPHA_FILT_OUT_FILE file!\n");
					exit(1);
				}
		}

		if (SAVE_HC5_ALPHA_AVG_POW == 1) {

			f_HC5_AVG_POW = fopen(HC5_ALPHA_AVG_POW_FILE, "w");
				if (f_HC5_AVG_POW == NULL) {
					printf("Error opening HC5_ALPHA_FILT_OUT_FILE file!\n");
					exit(1);
				}
		}


		if (SAVE_VOLUME == 1) {

			f_VOLUME = fopen(VOLUME_FILE, "w");
				if (f_VOLUME == NULL) {
					printf("Error opening VOLUME_FILE file!\n");
					exit(1);
				}
		}

		if (SAVE_SD_POW_D2_VC == 1) {

			f_POW_D2_VC = fopen(SD_POW_D2_VC_DATA_FILE, "w");
				if (f_POW_D2_VC == NULL) {
					printf("Error opening POW_D2_VC file!\n");
					exit(1);
				}
		}
}


void close_save_files(){

		if (SAVE_FILTERED_DATA_SD == 1){
			fclose(f_FILTER_D2);
			fclose(f_FILTER_D4);
			fclose(f_FILTER_B2);
			fclose(f_FILTER_MP);
		}

		if (SAVE_SLEEP_DETECTION == 1)
			fclose(f_SD);
		if (SAVE_SD_POW_D2 == 1)
			fclose(f_SD_POW_D2);
		if (SAVE_SD_POW_D4 == 1)
			fclose(f_SD_POW_D4);
		if (SAVE_SD_POW_B2 == 1)
			fclose(f_SD_POW_B2);
		if (SAVE_SD_POW_MP == 1)
			fclose(f_SD_POW_MP);

		if (SAVE_SW_DETECTION == 1)
			fclose(f_SWD);
		if (SAVE_SW_TH == 1)
			fclose(f_SW_TH);
		if (SAVE_SW_FILTERED_DATA == 1)
			fclose(f_SW_FILT);


		if (SAVE_PHASE_DETECTION == 1)
			fclose(f_PD);
		if (SAVE_PLL_ARGUMENT == 1)
			fclose(f_PLL_ARG);
		if (SAVE_PLL_FILTERED_DATA == 1)
			fclose(f_PLL_INP);

		if (SAVE_STIM_CONDITION == 1)
			fclose(f_SD);

		if (SAVE_HARD_COND_1 == 1)
			fclose(f_HARD_COND1);
		if (SAVE_HARD_COND_2 == 1)
			fclose(f_HARD_COND2);
		if (SAVE_HARD_COND_3 == 1)
			fclose(f_HARD_COND3);
		if (SAVE_HARD_COND_4 == 1){
			fclose(f_HARD_COND4);
			fclose(f_HC4_AVG_TH);
		}
		if (SAVE_HARD_COND_5 == 1){
			fclose(f_HARD_COND5);
			fclose(f_HC5_AVG_TH);
		}
		if (SAVE_HARD_COND_6 == 1){
			fclose(f_HARD_COND6);
		}

		if (SAVE_HC4_BETA_FILT_OUT == 1)
			fclose(f_HC4_FILT_OUT);
		if (SAVE_HC4_BETA_AVG_POW == 1)
			fclose(f_HC4_AVG_POW);

		if (SAVE_HC5_ALPHA_FILT_OUT == 1)
			fclose(f_HC5_FILT_OUT);
		if (SAVE_HC5_ALPHA_AVG_POW == 1)
			fclose(f_HC5_AVG_POW);

		if (SAVE_VOLUME == 1)
			fclose(f_VOLUME);

		if (SAVE_SD_POW_D2_VC == 1) {
			fclose(f_POW_D2_VC);
		}

}

/*
 * Get Raw data from file (.txt file containing the measured EEG raw data)
 */
float * getRawData(char *filename) {

	int j = 0;
	float value;
	int ch = 0;
	dataLen = 0;
	FILE *f;

	f = fopen(filename, "r"); /* Open a file from current drive. */

	if (f == NULL) {
		printf("ERROR: Raw data file not found! Check filename and path, and that file exists \n");
		exit(-1);
	}

	while (!feof(f)) {
		ch = fgetc(f);
		if (ch == '\n')
			dataLen++;
	}

	f = fopen(filename, "r"); /* Open a file from current drive. */
	float *output_file = malloc(sizeof(float) * dataLen);
	while (!feof(f) && fscanf(f, "%f", &value) && j++ < dataLen)
		output_file[j - 1] = (float) value;

	fclose(f);

	return output_file;
}

int main(void) {

	// *** Variables initialization ***********
	// Flags for stimulation condition
	int SLEEP_DETECTION 		= 0;
	int SW_DETECTION 			= 0;
	int PHASE_TARGET_DETECTION 	= 0;
	int STIM_CONDITION 			= 0;

	unsigned int sample_indx = 0;

	// Open Files for debugging
	if (DEBUGGING_ON == 1){
		open_save_files();
	}

	float *EEG_raw_data;
	float *EEG_raw_data_ref;
	float EEG_RawData;

	printf("%s\n\n",DATA_LOAD_NAME);
	// Get raw data from file. In case of device operation this needs to be replaced by the data in from device
	puts("Loading EEG data ...");
	char *filename = LOAD_CHANNEL_IN;


	//if (LOAD_DATA_FROM_KISPI==1){
		//char *filename_i   = LOAD_CHANNEL_STIM;
		//char *filename_ref = LOAD_CHANNEL_REF;
		//EEG_raw_data = getRawData(filename_i);
		//EEG_raw_data_ref = getRawData(filename_ref);
	//}else{
		EEG_raw_data = getRawData(filename);
	//}
	printf("%s %d\n\n","Data Length: ",dataLen);

	// Counters
	static uint32_t sleep_onset_counter = 0;
	static uint32_t rem_awk_counter = MAX_REM_AWK_TIME_SAMP;

	// Hard Conditions flags
	static bool INIT_FLAG = 0;
	static bool SD_ONSET_FLAG = 0;
	static bool HC2_FLAG = 1;
	static bool HC3_FLAG = 1;
	static bool HC_ARAUSAL = 1; // Hard condition 4
	float SWD_Filt_out = 0;

	static bool RESTART_COND = 0;

	static int ArausalDetCond;
	static int volume = INITIAL_VOLUME;


	static bool return_value = 0;
	static bool first_tone_flag = 0;
	static uint32_t ITI_counter = 0;

//	if((ENABLE_HARD_COND_4==1)&&(ENABLE_HARD_COND_5==1)){ArausalDetCond = 3; printf("%s\n\n","Arausal detection condition: Alpha and Beta.");}
//	else{
		if (ENABLE_HARD_COND_4==1){ArausalDetCond = 1; printf("%s\n\n","Arausal detection condition: Beta.");}
//		else{
			//if (ENABLE_HARD_COND_5==1){ArausalDetCond = 2; printf("%s\n\n","Arausal detection condition: Alpha.");}
			else{ArausalDetCond = 0; printf("%s\n\n","Arausal detection condition disable");}
		//}
	//}


	// Loop
	while (sample_indx < dataLen) {


		//if (LOAD_DATA_FROM_KISPI==1){
			//EEG_RawData = EEG_raw_data[sample_indx] - EEG_raw_data_ref[sample_indx];
		//}else{
			EEG_RawData = EEG_raw_data[sample_indx];
		//}
		sample_indx = sample_indx + 1;


		if (LOAD_DATA_FROM_KISPI==1){
			EEG_RawData = EEG_RawData*(1);
		}else{
			EEG_RawData = EEG_RawData*(-1); // inversion of data Values.
			EEG_RawData = NotchFilter_50Hz(EEG_RawData); // 50 Hz notch filter to remove line noise
		}


		//printf("%f%s", EEG_RawData, ", ");

		//printf("%f%s", EEG_RawData, ", ");
		/*****************************************************
		* Algorithms validation
		*****************************************************/

		// State machine: 2 main states: 1) REM_AWAKE 2) NREM
		// Code that runs always, not dependend on sleep stage detection
		SLEEP_DETECTION = NREM_SleepDetection_main(EEG_RawData);

		if(SWD_METHOD==1){SWD_Filt_out = SWD_Power_FiltData(EEG_RawData);
			//printf("%f\n", SWD_Filt_out);
		}
		if(ENABLE_HARD_COND_3==1){HC3_FLAG = HC_3_MovementDetection_FromRawData(EEG_RawData);} // Hard condition 3

		switch(SLEEP_DETECTION){

		case 0: // REM_AWAKE STATE

			SW_DETECTION   	= 0;
			PHASE_TARGET_DETECTION = 0;

			if (ENABLE_HARD_COND_1 == 1){
				rem_awk_counter++;

				// Hard condition 1
				RESTART_COND = rem_awk_counter>MAX_REM_AWK_TIME_SAMP;

				if (RESTART_COND == 1){
					if (SAVE_SW_FILTERED_DATA == 1)
						fprintf(f_SW_FILT, "%f%s ", 0.0, ",");
					if (SAVE_SW_TH == 1)
						fprintf(f_SW_TH, "%f%s ", 0.0, ",");
					if (SAVE_PLL_ARGUMENT == 1)
						fprintf(f_PLL_ARG, "%f%s", 0.0, ", ");
					if (SAVE_PLL_FILTERED_DATA == 1)
						fprintf(f_PLL_INP, "%f%s", 0.0, ", ");

					sleep_onset_counter = 0;

				}else{
					// wile no restart, continue running the algorithms
					sleep_onset_counter++;
					SW_DETECTION   	= SlowWaveDetector(EEG_RawData, sleep_onset_counter, RESTART_COND, SWD_Filt_out);
					PHASE_TARGET_DETECTION = HC_PLL(EEG_RawData, RESTART_COND, SWD_Filt_out, STIM_CONDITION, SW_DETECTION, SLEEP_DETECTION, HC2_FLAG);
				}

			}else{
				sleep_onset_counter = 0;
				RESTART_COND = 1;
				ITI_counter = 0;

				if (SAVE_SW_FILTERED_DATA == 1)
					fprintf(f_SW_FILT, "%f%s ", 0.0, ",");
				if (SAVE_SW_TH == 1)
					fprintf(f_SW_TH, "%f%s ", 0.0, ",");
				if (SAVE_PLL_ARGUMENT == 1)
					fprintf(f_PLL_ARG, "%f%s", 0.0, ", ");
				if (SAVE_PLL_FILTERED_DATA == 1)
					fprintf(f_PLL_INP, "%f%s", 0.0, ", ");

			}

			break;


		case 1: // SLEEP_DETECTION

			sleep_onset_counter++;
			rem_awk_counter = 0;    //CHECH IF IT IS CONVENIENT TO INITIALIZE THIS COUNTER AFTER SLEEP ONSET OR NOT

			// SWD and PLL runs during sleep onset time for filter stabilization
			SW_DETECTION   		   = SlowWaveDetector(EEG_RawData, sleep_onset_counter, RESTART_COND, SWD_Filt_out);
			PHASE_TARGET_DETECTION = HC_PLL(EEG_RawData, RESTART_COND, SWD_Filt_out,STIM_CONDITION, SW_DETECTION, SLEEP_DETECTION, HC2_FLAG);

			// Sleep onset hard condition
			INIT_FLAG = HC_0_SleepOnsetDelayCondition(sleep_onset_counter, INIT_FLAG);
			SD_ONSET_FLAG = HC_1_RemSleepOnsetDelayCondition(sleep_onset_counter, INIT_FLAG);

			RESTART_COND = 0;

			break;

		}

		// Stimulation on or off according to SWD, PLL and SD onset time condition
		STIM_CONDITION = (SW_DETECTION*PHASE_TARGET_DETECTION*SD_ONSET_FLAG*SLEEP_DETECTION)==1;

/*		 if(STIM_METHOD == 1){
			ITI_counter++;
			if(ITI_counter>=INTER_TONE_INTERVAL){
				if ((SW_DETECTION*SD_ONSET_FLAG*SLEEP_DETECTION)==1){
					STIM_CONDITION = 1;
					ITI_counter = 0;
				}else{
					STIM_CONDITION = 0;
				}

			}
			else
				STIM_CONDITION =  0;

		}*/


		// HARD CONDITIONS IMPLEMENTATION
		// Hard condition 3
		if(ENABLE_HARD_COND_3==1){STIM_CONDITION =  STIM_CONDITION*HC3_FLAG;};
		// Hard condition 2
		//if(ENABLE_HARD_COND_2==1){STIM_CONDITION = HC_2_ConsecutiveTones(STIM_CONDITION, SW_DETECTION, INIT_FLAG, RESTART_COND, sleep_onset_counter);}
		if(ENABLE_HARD_COND_2==1){
			HC2_FLAG = HC_2_ConsecutiveTones(STIM_CONDITION, SW_DETECTION, INIT_FLAG, RESTART_COND, sleep_onset_counter);
			STIM_CONDITION = STIM_CONDITION*!HC2_FLAG;
		}
		// Hard condition 4
		if(ArausalDetCond>0){ // Arousal detection condition > 0 implies that any of the arousal detection condition has been enabled
			HC_ARAUSAL = HC_4_BetaArausalDetection(EEG_RawData, INIT_FLAG);
			STIM_CONDITION =  STIM_CONDITION*HC_ARAUSAL;
		}
		/****************************************************
		 * VOLUME CONTROL
		 ****************************************************/
		volume = VolumeCtrlArousal(SLEEP_DETECTION, STIM_CONDITION, HC_ARAUSAL); // NOTE: Needs to be finished

		/****************************************************
		 * DEBUGGING
		 ****************************************************/
		if (DEBUGGING_ON == 1){
			if (SAVE_SLEEP_DETECTION == 1)
				fprintf(f_SD, "%d%s ", SLEEP_DETECTION, ",");
			if (SAVE_SW_DETECTION == 1)
				fprintf(f_SWD, "%d%s ", SW_DETECTION, ",");
			if (SAVE_STIM_CONDITION == 1)
				fprintf(f_STIM_COND, "%d%s ", STIM_CONDITION, ",");
			if (SAVE_PHASE_DETECTION == 1)
				fprintf(f_PD, "%d%s ", PHASE_TARGET_DETECTION, ",");


			if (SAVE_HARD_COND_1 == 1)
				fprintf(f_HARD_COND1, "%d%s ", SD_ONSET_FLAG, ",");

			if (SAVE_VOLUME == 1)
				fprintf(f_VOLUME, "%d%s ", volume, ",");
		}

	}

	if (DEBUGGING_ON == 1)
		close_save_files();

	printf("%s\n\n", "********* Done! *********");
}

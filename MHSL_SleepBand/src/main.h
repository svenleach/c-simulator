/*
 * ==========================================================================================================
 * Name        : main.h
 * Author      : Maria Laura Ferster
 * Version     : 1.0
 * Date		   : 06-June-2017
 * Copyright   : (c) 2017.  Maria Laura Ferster, Mobile Health Systems Lab, ETH Zurich. All rights reserved
  ============================================================================================================
 */

#ifndef MAIN_H_
#define MAIN_H_

// My includes
#include <stdio.h>                      /* Standard I/O .h-file               */
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>
//#include <cmath>

//#include "waveplayer.h"

#include "utils.h"
#include "config.h"
#include "logger.h"

// Deine 0
#ifndef NULL
#define NULL   ((void *) 0)
#endif

// global variables to save filter data
FILE *f_FILTER_A;
FILE *f_FILTER_B;
FILE *f_FILTER_D;
FILE *f_FILTER;
FILE *f_FILTER_PRE;

FILE *f_FILTER_D2;
FILE *f_FILTER_D4;
FILE *f_FILTER_B2;
FILE *f_FILTER_MP;

FILE *f_SD;
FILE *f_SD_POW_D2;
FILE *f_SD_POW_D4;
FILE *f_SD_POW_B2;
FILE *f_SD_POW_MP;

FILE *f_SWD;
FILE *f_SW_TH;
FILE *f_SW_FILT;
FILE *f_SWD_PWR;

FILE *f_PD;
FILE *f_PLL_ARG;
FILE *f_PLL_INP;

FILE *f_STIM_COND;

FILE *f_HARD_COND1;
FILE *f_HARD_COND2;
FILE *f_HARD_COND3;
FILE *f_HARD_COND4;
FILE *f_HARD_COND5;
FILE *f_HARD_COND6;

FILE *f_HC4_FILT_OUT;
FILE *f_HC4_AVG_POW;
FILE *f_HC4_AVG_TH;

FILE *f_HC5_FILT_OUT;
FILE *f_HC5_AVG_POW;
FILE *f_HC5_AVG_TH;

FILE *f_VOLUME;
FILE *f_POW_D2_VC;


#endif /* MAIN_H_ */

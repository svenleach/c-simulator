/*
 * ==========================================================================================================
 * Name        : sleep_detector2.c
 * Author      : Maria Laura Ferster
 * Version     : 1.0
 * Date		   : 17-June-2017
 * Copyright   : (c) 2017.  Maria Laura Ferster, Mobile Health Systems Lab, ETH Zurich. All rights reserved
 *
 * Description : NREM sleep detection function.
 *
 * Objective   : Detect the NREM sleep phase. It does not distinguish between the different phases at NREM
 *
 ============================================================================================================
 */


#include "sleep_detector2.h"
#include "filters.h"
#include "utils.h"
#include <stdio.h>



/*
 * NREM Sleep detection
 */


int NREM_SleepDetection_main(float inEEGdata){

	// ***** Initialization START ********************
	static uint8_t NREM_SD_FLAG = 0;
	static bool initialized;

	// coefficients
	static float Ndelta2[5];
	static float Ddelta2[5];
	static float Ndelta4[5];
	static float Ddelta4[5];
	//static float NmuscPow2[5];
	//static float DmuscPow2[5];

	// input buffer
	float static INPUTS[5];

	// output (data filtered) buffer
	float static delta2FiltOut[5];
	float static delta4FiltOut[5];
	float static muscPow2FiltOut[5];

	// for power and moving average calculation
	static int pow_counter = 0;
	static float delta2PowerAccumulator=0;
	static float delta4PowerAccumulator=0;
	static float musclePowerAccumulator=0;


	static float delta2Power;
	static float delta4Power;
	static float musclePower2;

	static float delta4PowerWindow[AVG_CALC_SAMP]; // sums up last 19 values, starts with 19, 18, 17... (only give 1 number since it is just summing up, shown by a + after var before 					deltaPower)
	static float delta2PowerWindow[AVG_CALC_SAMP];
	static float musclePowerWindow[AVG_CALC_SAMP];

	float D4overM;

	//****** Debbuging ******************************
	#define deb_buffer_size 250
	//static float D4overM_v[deb_buffer_size];
	//static uint32_t deb_indx=0;

	// ***** Initialization ENDS ********************


	// ***** Get filtered data START *******************
	//Coefficient calculation
	if (!initialized) {
		float *Coeff;
		//printf("%s\n\n","SD filters initialization..");
		Coeff = CoeffCalculation(LowFcDelta2, HighFcDelta2);
		for (int i = 0; i < 5; i++){
			Ndelta2[i] = Coeff[i];
			Ddelta2[i] = Coeff[i+5];
			//printf("%s%f%s%f\n","N: ",Ndelta2[i]," - D: ",Ddelta2[i]);
		}
		Coeff = CoeffCalculation(LowFcDelta4, HighFcDelta4);
		for (int i = 0; i < 5; i++){
			Ndelta4[i] = Coeff[i];
			Ddelta4[i] = Coeff[i+5];
		}
		/*Coeff = CoeffCalculation(LowFcMuscPow2, HighFcMuscPow2);
		for (int i = 0; i < 5; i++){
			NmuscPow2[i] = Coeff[i];
			DmuscPow2[i] = Coeff[i+5];
		}*/
	    initialized = true;
	}

	// buffer shift
	for (int i = 1; i < 5; i++){
		INPUTS[5-i]          = INPUTS[4-i];// So loop with 5 steps is done in 1 period (1 sample!)
		delta2FiltOut[5-i]   = delta2FiltOut[4-i];
		delta4FiltOut[5-i]   = delta4FiltOut[4-i];
		muscPow2FiltOut[5-i] = muscPow2FiltOut[4-i];
	}
	INPUTS[0]=inEEGdata;
	delta2FiltOut[0]   = 0;
	delta4FiltOut[0]   = 0;
	muscPow2FiltOut[0] = 0;

	// filter data
	delta2FiltOut[0]   = Butter2ndOrder(INPUTS, delta2FiltOut, Ndelta2, Ddelta2);
	delta4FiltOut[0]   = Butter2ndOrder(INPUTS, delta4FiltOut, Ndelta4, Ddelta4);
	//muscPow2FiltOut[0] = Butter2ndOrder(INPUTS, muscPow2FiltOut, NmuscPow2, DmuscPow2);
	muscPow2FiltOut[0] = Muscle_6thOrder_filter(inEEGdata);

	if (SAVE_FILTERED_DATA_SD == 1) {
		fprintf(f_FILTER_D2, "%f%s ", delta2FiltOut[0], ",");
		fprintf(f_FILTER_D4, "%f%s ", delta4FiltOut[0], ",");
		fprintf(f_FILTER_MP, "%f%s ", muscPow2FiltOut[0], ",");
	}
	// ***** Get filtered data ENDS ***********


	// ***** MVA calculation START ***********
	// Power calculation
	if ( (pow_counter > 0) & (pow_counter < POW_CALC_WIND) ){ //sum power values while counting up to POW_CALC_WIND seconds

		delta2PowerAccumulator  += pow(delta2FiltOut[0],2)/POW_CALC_WIND;
		delta4PowerAccumulator  += pow(delta4FiltOut[0],2)/POW_CALC_WIND;
		musclePowerAccumulator  += pow(muscPow2FiltOut[0],2)/POW_CALC_WIND;

		pow_counter+=1;

	}else if (pow_counter==0){ //initialize power value summations, start counting up to 1 second (after first after 1 has been reached)

		delta2PowerAccumulator = pow(delta2FiltOut[0],2)/POW_CALC_WIND;
		delta4PowerAccumulator = pow(delta4FiltOut[0],2)/POW_CALC_WIND;
		musclePowerAccumulator = pow(muscPow2FiltOut[0],2)/POW_CALC_WIND;

		pow_counter+=1;

	}else{ //whenever counter has finished (at sample number fs), calculate the full average power

		pow_counter = 0; // Starts power calculation counter from 0 to again run until POW_CALC_WIND

		delta2Power  = 0;
		delta4Power  = 0;
		musclePower2 = 0;

		//*** Compute moving window average of the POW_CALC_WIND_SEC average power values in an epoch of AVG_CALC_WIND_SEC seconds//

		delta4PowerAccumulator += pow(delta4FiltOut[0],2)/POW_CALC_WIND; // Defines power value for the current second by adding the last sample (see if else before)
		delta2PowerAccumulator += pow(delta2FiltOut[0],2)/POW_CALC_WIND;
		musclePowerAccumulator += pow(muscPow2FiltOut[0],2)/POW_CALC_WIND;

		// Buffer shift
		for (int i=1; i<AVG_CALC_SAMP;i++){
			delta4PowerWindow[AVG_CALC_SAMP-i] = delta4PowerWindow[AVG_CALC_SAMP-1-i];
			delta2PowerWindow[AVG_CALC_SAMP-i] = delta2PowerWindow[AVG_CALC_SAMP-1-i];
			musclePowerWindow[AVG_CALC_SAMP-i] = musclePowerWindow[AVG_CALC_SAMP-1-i];
		}

		delta4PowerWindow[0] = delta4PowerAccumulator*AVG_GAIN/(float)AVG_CALC_SAMP; // Actualization of the last number
    	delta2PowerWindow[0] = delta2PowerAccumulator*AVG_GAIN/(float)AVG_CALC_SAMP;
	    musclePowerWindow[0] = musclePowerAccumulator*AVG_GAIN/(float)AVG_CALC_SAMP;

		for (int i=0; i<AVG_CALC_SAMP;i++){
			delta4Power  += delta4PowerWindow[AVG_CALC_SAMP-i]; // sums up last AVG_CALC_SAMP values
			delta2Power  += delta2PowerWindow[AVG_CALC_SAMP-i];
			musclePower2 += musclePowerWindow[AVG_CALC_SAMP-i];
		}

	// ***** MVA calculation END ***********


	// ***** Sleep detection based on MVA calculation START ***********
		D4overM = delta4Power / musclePower2;

		/*if (deb_indx<deb_buffer_size){
			printf("%f%s ", D4overM, "\n");
			deb_indx++;
		}*/

		float NREM_delta2 = NREM_Delta2_Transfer(delta2Power, 0);

		if (POPULATION==0){ //check delta2

			if ( (D4overM > Y_D4_OVER_M_TH) & (delta2Power > Y_D2_TH) & (musclePower2 < Y_Muscle_TH))
				NREM_SD_FLAG = 1;
			else
				NREM_SD_FLAG = 0;

		}else if (POPULATION==1){ // sleep detection for elderly
			if ( (D4overM > E_D4_OVER_M_TH) & (delta2Power > E_D2_TH) & (musclePower2 < E_Muscle_TH))
				NREM_SD_FLAG = 1;
			else
				NREM_SD_FLAG = 0;
		}else{

			printf("ERROR!!! POPULATION input value not valid. Check config.h and insert a valid definition \n");
			printf("0: for young adults. 1: for elderly \n");
			exit(1);
		}
	}

	if (SAVE_SD_POW_D2 == 1)
		fprintf(f_SD_POW_D2, "%f%s ", delta2Power, ",");
	if (SAVE_SD_POW_D4 == 1)
		fprintf(f_SD_POW_D4, "%f%s ", delta4Power, ",");
	if (SAVE_SD_POW_MP == 1)
		fprintf(f_SD_POW_MP, "%f%s ", musclePower2, ",");


	// ***** Sleep detection based on MVA calculation END ***********

	return NREM_SD_FLAG;
}





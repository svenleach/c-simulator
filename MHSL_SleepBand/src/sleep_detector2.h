/*
 * ==========================================================================================================
 * Name        : sleep_detector2.h
 * Author      : Maria Laura Ferster
 * Version     : 1.0
 * Date		   : 15-June-2017
 * Copyright   : (c) 2017.  Maria Laura Ferster, Mobile Health Systems Lab, ETH Zurich. All rights reserved
 ============================================================================================================
 */
  
#ifndef  SLEEP_DETECTOR2_H
#define SLEEP_DETECTOR2_H

#include "main.h"
#include "filters.h"

int NREM_SleepDetection_main(float inEEGdata);


#endif //!defined 

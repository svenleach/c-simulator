/*
 * ==========================================================================================================
 * Name        : tone_generator.c
 * Author      : Maria Laura Ferster
 * Version     : 1.0
 * Date		   : 20-June-2017
 * Copyright   : (c) 2017.  Maria Laura Ferster, Mobile Health Systems Lab, ETH Zurich. All rights reserved
 *
 * Description : Generates pink noise array
 *
 ============================================================================================================
 */

#define _USE_MATH_DEFINES

// TODO: reference additional headers your program requires here
//#include <iostream>
#include "tone_generator.h"
#include <stdio.h>
//#include <algorithm>
//#include <vector.h>
#include <stdlib.h>


//#include <valarray>

#include "main.h"
	
//using namespace std;

double * linspace(double a, double b, int n) {

	double *array;
	if ((n == 0) || (n == 1) || (a == b))
		array = &b;
	else if (n > 1) {
		double step = (b - a) / (n - 1);
		
		int count = 0;		
		for (count=0; count<n; count++ )
			array[count] = a + count*step;
	}
	return array;
}


double complex *dft_func(double complex *data, double complex **exp_Mx) {


	double complex *dft;
	//dft = new complex<double>[NumSamp];

	for (int row = 0; row < TONE_DURATION_IN_SAMP; row++) {
		for (int col = 0; col < TONE_DURATION_IN_SAMP; col++) {
			dft[row] += exp_Mx[row][col] * data[row];
		}
	}

	return dft;
}

double complex *ifft_func(double complex *data, double complex **exp_Mx) {

	double complex *ifft_out;
	//ifft_out = new complex<double>[TONE_DURATION_IN_SAMP];

	for (int row = 0; row < TONE_DURATION_IN_SAMP; row++) {
		data[row] = conj(data[row]);
	}

	ifft_out = dft_func(data, exp_Mx);

	for (int row = 0; row < TONE_DURATION_IN_SAMP; row++) {
		ifft_out[row] = conj(ifft_out[row]);
		//ifft_out[row].real = ifft_out[row].real / NumSamp;
		//ifft_out[row].imag = ifft_out[row].imag / NumSamp;
	}

	return ifft_out;
}

double * reverse(double * array_for_reverse){

	int array_len = sizeof(array_for_reverse);
	double *array_reversed;
	int i;
	
	for (i=0; i<array_len; i++)
		array_reversed[array_len-1-i] = array_for_reverse[i];
	return array_reversed;
}


void pinkNoise(double complex ** Matrix) {

	double* L_freq;
	double* H_freq;	
	double* phase_vals;
	
	// Frequency vector
	L_freq  = linspace(0.0, 0.5, floor(TONE_DURATION_IN_SAMP / 2));
	
	H_freq = linspace(0.0, 0.5, ceil(TONE_DURATION_IN_SAMP / 2));
	H_freq = reverse(H_freq);
	
	double* freqs = malloc((sizeof(L_freq)+sizeof(H_freq)) * sizeof(double)); // array to hold the result
	memcpy(freqs,     L_freq, sizeof(L_freq) * sizeof(double)); 							// copy the low frequencies first
	memcpy(freqs + sizeof(L_freq), H_freq, sizeof(H_freq) * sizeof(double));  // copy the high frequencies	
		
	// free memory
	free(L_freq);
	free(H_freq);
	
	for (int i = 0; i < sizeof(freqs); i++) {
		freqs[i] = pow(pow(freqs[i], 2), -0.5); // 1/sqrt(freq^2)
		if (isinf(freqs[i]))
			freqs[i] = 0.0;
	}

	// Phase vector
	for (int i = 0; i < TONE_DURATION_IN_SAMP; i++) {
		phase_vals[i] = (double)rand() / ((double)RAND_MAX + 1.0);
	}
	
	
	/*
	% generate tone on the frequency domain: Matlab code
	phase_vals = rand(num_samples,1); % randome phase values
	tone_fft = sqrt(freqs).*(cos(2*pi*phase_vals)+1i*sin(2*pi*phase_vals));
	%now apply an inverse fft:
	tone = ifft(tone_fft);
	tone = real(tone);
	tone = tone./max(abs(tone)); %normalize to +/- 1.
	*/	

	double complex* tone_fft;
	double complex* tone_ifft;
		
	double complex Re;
	double complex Im;
	
	double normalization_factor;
	double tone;
	
	for (int i = 0; i < TONE_DURATION_IN_SAMP; i++){
		
		Re = pow(freqs[i], 0.5)*cos(2 * M_PI*phase_vals[i]);
		Im = pow(freqs[i], 0.5)*sin(2 * M_PI*phase_vals[i])*pow(-1,2);
		tone_fft[i] =  Re + Im;
		
	}	
	
	//Apply the inverse fft
	tone_ifft = dft_func(tone_fft, Matrix);
	
	// Not sure
	tone = creal(*tone_ifft);
		
	// abs and max
	for (int i=0; i<sizeof(tone); i++){}
		//normalization_factor = fmax(fabs(tone));

	// free memory
	free(tone_ifft);
	free(tone_fft);
}



int main_func(){

	// complex matrix for pink tone generation
	double complex **Matrix;
	//Matrix = new complex<double> *[TONE_DURATION_IN_SAMP];
	
	for (int row = 0; row < TONE_DURATION_IN_SAMP; row++) {
		for (int col = 0; col < TONE_DURATION_IN_SAMP; col++)			
			Matrix[row][col] = cos(M_2PI*row*col / TONE_DURATION_IN_SAMP) + -sin(M_2PI*row*col / TONE_DURATION_IN_SAMP)*pow(-1,2);
	}
	
	pinkNoise(Matrix);

	//cin.get();
	//system("pause");

	return 0;
}


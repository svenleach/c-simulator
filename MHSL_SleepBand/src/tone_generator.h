/*
 * ==========================================================================================================
 * Name        : tone_generator.h
 * Author      : Maria Laura Ferster
 * Version     : 1.0
 * Date		   : 20-June-2017
 * Copyright   : (c) 2017.  Maria Laura Ferster, Mobile Health Systems Lab, ETH Zurich. All rights reserved
 ============================================================================================================
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef TONE_GENERATOR_H 
#define TONE_GENERATOR_H

#include "config.h"
#include "main.h"

double * linspace(double a, double b, int n);
double * reverse(double * array_for_reverse);
	
double complex *dft_func(double complex *data, double complex **exp_Mx);
double complex *ifft_func(double complex *data, double complex **exp_Mx); 

	
void pinkNoise(double complex ** Matrix);
int main_func();

#endif //!defined

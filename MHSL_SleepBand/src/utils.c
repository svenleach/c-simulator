/*
 * ==========================================================================================================
 * Name        : utils.c
 * Author      : Maria Laura Ferster
 * Version     : 1.0
 * Date		   : 05-October-2017
 * Copyright   : (c) 2017.  Maria Laura Ferster, Mobile Health Systems Lab, ETH Zurich. All rights reserved
 *
 * Description : functions, constants, and definitions that can be used by any module
 *
 * ============================================================================================================
 */
	
#include "utils.h"


float rms(float *v, float rms_window_size) {
	int i;
	float sum = 0.0;
	float n = rms_window_size;
	for (i = 0; i < n; i++)
		sum += (float)v[i] * (float)v[i];
	sum = sqrt(sum / n);
	return sum;
}

float NREM_Delta2_Transfer(float NREM_Delta2, bool condition){

	static float ret_value;
	static float delta2_POW;

	if (condition==0){
		delta2_POW = NREM_Delta2;
		ret_value = 0.0;
	}else{
		ret_value = delta2_POW;
	}

	return ret_value;

}



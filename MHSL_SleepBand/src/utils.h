/*
 * ==========================================================================================================
 * Name        : utils.h
 * Author      : Maria Laura Ferster
 * Version     : 1.0
 * Date		   : 05-October-2017
 * Copyright   : (c) 2017.  Maria Laura Ferster, Mobile Health Systems Lab, ETH Zurich. All rights reserved
 * ============================================================================================================
 */

#include <math.h>
#include <stdbool.h>

/* PARAMETERS */
#define M_2PI 	3.14159265358979323846*2
#define M_PI 	3.14159265358979323846
#define M_PI_2	1.57079632679489661923


/* FUNCTIONS */

float rms(float *v, float rms_window_size);
float NREM_Delta2_Transfer(float NREM_Delta2, bool condition);
/* GLOBAL VARIABLES */

//static float NREM_delta2;

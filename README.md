# README - C_Simmulator #

### What is this repository for? ###

* Simulation of MHSL-SleepBand algorithms before implementing on hardware
* Testing of new algorihms peroformance
* Testing of implemented algorithms' performance on different data sets
* Testing algorithms' performance while changing the parameters value


### How do I get set up? ###

To install all the required programs and start read C_Simulator_guide.pdf

### Folders Structure ###

**MHSL_SleepBand**: contains src code of the C simulator 
**RunningC**: contains the scripts to variate parameters and run the C simulator over a Matlab interface

### Who do I talk to? ###

Maria Laura Ferster - maria.ferster@hest.ethz.ch
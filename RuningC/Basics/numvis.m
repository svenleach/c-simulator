% NUMVIS
%-----------------------------------------------------------------------------
%
% Converts VisName obtained with READTRAC into a numeric array: nREM sleep stages < 0,
% REM Sleep 0 Wake 1 Movement 2 others 3
%
%-----------------------------------------------------------------------------
function [VisNum] = numvis(VisSymb,Offset);

VisNum=zeros(1,size(VisSymb,2)-Offset);
VisNum=VisNum+3;
Index=find(VisSymb=='1')-Offset;
VisNum(Index)=-1;
Index=find(VisSymb=='2')-Offset;
VisNum(Index)=-2;
Index=find(VisSymb=='3')-Offset;
VisNum(Index)=-3;
Index=find(VisSymb=='4')-Offset;
VisNum(Index)=-4;
Index=find(VisSymb=='r')-Offset;
VisNum(Index)=0;
Index=find(VisSymb=='0')-Offset;
VisNum(Index)=1;
Index=find(VisSymb=='m')-Offset;
VisNum(Index)=2;

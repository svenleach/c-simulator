% function C_Parameters_Configuration(filePath, fileName, var_name, var_value)
% C_Parameters_Configuration - generated the parameter changes on config
% and logger file
%
% Syntax:  C_Parameters_Configuration(filePath, fileName, var_name, var_value)
%
% Inputs:
%    filePath  - path of the configuration/logger file
%    fileName  - config.h or logger.h file
%    var_name  - name of the variable value to be changed
%    var_value - value corresponding to the var_name
%
% Authors with email: Maria Laura Ferster (maria.ferster@hest.ethz.ch)
% Date of creation: 29.10.2017
% Version: v0.1
% Validation date/version: 30.10.2017
% Validated by: Laura Ferster
% Copyright: (c) 2017.  MARIA LAURA FERSTER, Mobile Health Systems Lab, ETH Zurich. All rights reserved

%------------- BEGIN CODE --------------
function C_Parameters_Configuration(filePath, fileName, var_name, var_value)

variable_name = ['#define ',var_name];

% extract from file the information per line
fileInfo = [filePath,fileName];
A = regexp( fileread(fileInfo), '\n', 'split');

% find the line containing the variable_name
B=strfind(A, variable_name); 
fileSize = length(A);

index = [];
for k=1:fileSize
    if B{k}>=1;
        index = [index k];
    end
end

if length(index)==1
    A{index} = sprintf('%s',[variable_name,'   ',num2str(var_value)]);
else
    % if more that one file containing the variable_name is found, change the firts one.
    A{index(1)} = sprintf('%s',[variable_name,'   ',num2str(var_value)]);
end

fid = fopen(fileInfo,'w'); % open file and re write with the changes.
fprintf(fid, '%s\n', A{:});
fclose(fid);    %close file

end

%------------- END OF CODE --------------
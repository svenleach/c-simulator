clc
clearvars
close all

scriptPath = 'C:\Scripts\Epiversum\prepare-study\stim-algorithm\EPISL_kids_epi';
mainPath   = 'C:\Scripts\Epiversum\prepare-study\stim-algorithm\EPISL_kids_epi';
run(fullfile(scriptPath, 'config.m'))

% System paths and commands to run simulation
filePath = '..\MHSL_SleepBand\src\';
sysPath  = '..\MHSL_SleepBand\Debug\';
ToBuild  = {'PLL', 'SW_detector', 'filters', 'main', 'sleep_detector2','HardConditions','iir','utils','VolumeControl'}; % All the names of functions needed to run the C simulator. Needed to compile 
sysRun   = 'MHSL_SleepBand.exe';    %Executable name
matPath  = pwd;

% Parameters
% *************************************************************************
S.TARGET_PHASE      = [-0.15];
S.GAIN_NCO          = [1];   
S.GAIN_PD           = [3200];   
S.Y_D4_OVER_M_TH    = [16];  
S.Y_D2_TH           = [700];   
S.Y_Muscle_TH       = [20]; 
S.SWD_PWR_THRESHOLD = [800];  
	

%--------------------------------------------------------------------------
% Initialization
%--------------------------------------------------------------------------

%-- Load data path: indicate the path where the data to be analyzed is
load_path = '"D:\\SIM\\SimEEG\\EpiKids\\C\\"'; %'"C:\\Users\\fersterm\\Desktop\\"';
C_Parameters_Configuration(filePath, 'logger.h', 'LOAD_DATA_PATH', load_path)

%-- save data path: indicate teh path where results are saved
save_path = '"D:\\SIM\\SimOUT\\EpiKids\\"'; % '"C:\\Users\\fersterm\\Desktop\\"';
C_Parameters_Configuration(filePath, 'logger.h', 'DATA_SAVE_PATH', save_path)

%-- Case_ID: name of file/s to be tested
subject_v = conf.subj.toSimulate;   % Data load name. Add more than one to run several subjects 
test_name = '';                     % Used to identify specific runing test on the saved data

%-- case initialization
pll_opt     = 0;    % Runs the pll optimization case
swd_opt     = 0;    % Runs the swd optimization case
default_run = 1;    % Runs a default case

%-- By default enabling/disabling saving data 
C_Parameters_Configuration(filePath,'logger.h', 'DEBUGGING_ON', 1)  %debbuging_on = 1 always if not it will not save data.

% SLEEP DETECTION
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_SLEEP_DETECTION', 1)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_FILTERED_DATA_SD', 1)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_SD_POW_D2', 1)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_SD_POW_D4', 1)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_SD_POW_B2', 1)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_SD_POW_MP', 1)

% SW DETECTION
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_SW_DETECTION', 1)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_SW_TH', 1)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_SW_FILTERED_DATA', 1)

%  PLL
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_PHASE_DETECTION', 1)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_PLL_ARGUMENT', 0)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_PLL_FILTERED_DATA', 1)
    
%  PLL
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_STIM_CONDITION', 1)

% HARD CONDITIONS
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_HARD_COND_1', 1)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_HARD_COND_2', 1)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_HARD_COND_3', 1)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_HARD_COND_4', 1)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_HARD_COND_5', 1)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_HC4_BETA_FILT_OUT', 1)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_HC4_BETA_AVG_POW', 1)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_HC5_ALPHA_FILT_OUT', 1)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_HC5_ALPHA_AVG_POW', 1) 


% Parameter optimization
% *************************************************************************

% find parameters with more than 1 value
ndxPar    = structfun(@length, S) > 1;        % index of fields with more than 1 value   
fldPar    = fieldnames(S);                    % list of parameter names
if all(ndxPar == 0)
    ndxPar(ndxPar == 0) = 1                   % In case of one simulation only
end
arrayPar  = fldPar(ndxPar);                   % list of parameter names to be optimized
singlePar = fldPar(~ndxPar);                  % list of parameter names to be set once

% Run all parameter combinations
% *************************************************************************
if ~all(ndxPar)

    % set single parameters
    for iSingle = 1:length(singlePar)

        par_name  = singlePar{iSingle};
        par_value = S.(par_name);
        C_Parameters_Configuration(filePath, 'config.h', par_name, par_value);
    end

    % write file so that you know what you did
    writetable(struct2table(S), fullfile(save_path(2:end-1), 'parameters.csv'))

    % Find all values for all parameters
    for iPar = 1:length(arrayPar)

        % Prepare "settings_str"
        str_var_opt = arrayPar{iPar};                  % Name of parameter that is to be optimized
        eval(['varray_var_opt = S.' str_var_opt ';']); % All values of the variable to be optmized

        % All values in a cell array
        par_val{iPar} = varray_var_opt;
    end

    % All combinations of values
    par_comb      = cell(1,numel(par_val));
    [par_comb{:}] = ndgrid(par_val{:});
    par_comb      = cellfun(@(X) reshape(X,[],1), par_comb, 'UniformOutput',false);
    par_comb      = horzcat(par_comb{:});

    % Display
    fprintf('The following parameters will be optmized with the following values:\n')
    for iPar = 1:length(arrayPar)

        % Show parameter names & values that are going to be optimized    
        array_val_par = sprintf(repmat('%d ', 1, length(par_val{iPar})), par_val{iPar});
        fprintf('#   %s: %s\n', arrayPar{iPar}, array_val_par);
    end
    fprintf('\n*** Number of Parameter combinations: %d \n', size(par_comb, 1))    

    % Show simulation path
    fprintf('\nYour simulations will be saved here:\n%s\n\n', save_path(2:end-1));


    % Run simulations for all combinations
    % *************************************************************************

    % Loop through all combinations
    for iComb = 1:size(par_comb, 1)

        % Filename
        test_name = [];  

        % Loop through all parameters
        for iPar = 1:length(arrayPar)

            % settings_str
            par_name  = arrayPar{iPar};         % Name of parameter that is to be optimized
            par_value = par_comb(iComb, iPar);  % Set value in "settings_str"

            % Prepare filename
            parID                                                   = par_name;
            parID(strfind(parID, 'Y_') : strfind(parID, 'Y_')+1)    = [];        
            parID                                                   = parID(1:3);
            parID(strfind(parID, '_'))                              = [];        
            test_name = sprintf('%s%s_%g_', test_name, parID, par_comb(iComb, iPar));   

            % Change .h file
            C_Parameters_Configuration(filePath, 'config.h', par_name, par_value)        

        end
        test_name = ['_' test_name];
        test_name(end) = []; % Remove last underscore   
        test_name = strrep(test_name, '-', 'm');
        test_name = strrep(test_name, '.', '_');    

        % Change logger file for subjects and run
        fprintf('*** Parameter combination %d/%d: %s ***\n', iComb, size(par_comb, 1), test_name)    
        Run_Configuration_on_Subjects(subject_v, test_name, ToBuild, sysRun, filePath)
    end
    
% Run only combindation of all 1st values, all 2nd values etc.
% *************************************************************************
else
    
    % Loop through all combinations
    nComb = structfun(@length, S);
    nComb = nComb(1);
    for iComb = 1:nComb

        % Filename
        test_name = [];  

        % Loop through all parameters
        for iPar = 1:length(arrayPar)

            % settings_str
            par_name  = arrayPar{iPar};      % Name of parameter that is to be optimized
            par_value = S.(par_name)(iComb); % Set value in "settings_str"

            % Prepare filename
            parID                                                   = par_name;
            parID(strfind(parID, 'Y_') : strfind(parID, 'Y_')+1)    = [];        
            parID                                                   = parID(1:3);
            parID(strfind(parID, '_'))                              = [];        
            test_name = sprintf('%s%s_%g_', test_name, parID, par_value);   

            % Change .h file
            C_Parameters_Configuration(filePath, 'config.h', par_name, par_value)        

        end
        test_name = ['_' test_name];
        test_name(end) = []; % Remove last underscore   
        test_name = strrep(test_name, '-', 'm');
        test_name = strrep(test_name, '.', '_');    

        % Change logger file for subjects and run
        fprintf('*** Parameter combination %d/%d: %s ***\n', iComb, nComb, test_name)    
        Run_Configuration_on_Subjects(subject_v, test_name, ToBuild, sysRun, filePath)
    end      
end



% Make mat file
% *************************************************************************
fprintf('Starting .mat files\n')
for iSub = 1:length(subject_v)
    fprintf('Subject %d/%d\n', iSub, length(subject_v))    
    subject = subject_v{iSub};
    path    = save_path(2:end-1);

    files_STIM   = dir(fullfile(path, [subject, '*STIM_CONDITION.txt']));
    files_SLEEP  = dir(fullfile(path, [subject, '*SLEEP_DETECTION.txt']));
    files_SW     = dir(fullfile(path, [subject, '*SW_DETECTION.txt']));
    files_PAHSE  = dir(fullfile(path, [subject, '*PHASE_DETECTION.txt']));
  
    parfor iFile = 1:length(files_STIM)
        filename_MAT = [files_STIM(iFile).name(1:end-19), '.mat'];        
        fprintf('File %d/%d: %s\n', iFile, length(files_STIM), filename_MAT)          

        if exist(fullfile(path, filename_MAT), 'file')
            continue;
        end        
        
        % Load T       
        fidT          = fopen(fullfile(path, files_STIM(iFile).name));
        T             = textscan(fidT, '%f', 'Delimiter',',');
        fclose(fidT);       
        
        % Load Sleep       
        fidSLEEP      = fopen(fullfile(path, files_SLEEP(iFile).name));
        SLEEP         = textscan(fidSLEEP, '%f', 'Delimiter',',');
        fclose(fidSLEEP);  
        
        % Load SW       
        fidSW         = fopen(fullfile(path, files_SW(iFile).name));
        SW            = textscan(fidSW, '%f', 'Delimiter',',');
        fclose(fidSW);  
        
        % Load Phase       
        fidPHASE      = fopen(fullfile(path, files_PAHSE(iFile).name));
        PHASE         = textscan(fidPHASE, '%f', 'Delimiter',',');
        fclose(fidPHASE);          
           
        FLG                 = [];
        FLG.stim_enabled    = logical(T{1});
        FLG.ph_detected     = logical(PHASE{1});
        FLG.sw_detected     = logical(SW{1});
        FLG.sleep_detected  = logical(SLEEP{1});
        
        % Save
        parsave(fullfile(path, filename_MAT), FLG);        
%         save(fullfile(path, filename_MAT), 'FLG')
    end
end

% Check simulations
cd 'C:\Scripts\Epiversum\prepare-study\eval-simulations'
run('evalSIM_C.m')
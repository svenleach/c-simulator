%% Name of script: C_Simulator_RUN_script
% The script allows to run the C simulator automatically while changing
% the desired parameter values. This case contains an example for PLL
% parameters analysis
% 
% other M-files required: 
%    C_Parameters_Configuration.m
%    Run_Configuration_on_Subjects.m  
%
% Toolboxes required: gcc installed to run C script
%
% Other programs required: 
%    Eclipse
%    C simulator program installed
%
% Authors with email: Maria Laura Ferster (maria.ferster@hest.ethz.ch)
% Date of creation: 28.10.2017
% Version: v0.1
% Validation date/version: 30.10.2017
% Validated by: Laura Ferster
% Copyright: (c) 2017.  MARIA LAURA FERSTER, Mobile Health Systems Lab, ETH Zurich. All rights reserved

%------------- BEGIN CODE --------------

%--------------------------------------------------------------------------
% System paths and commands to run simulation
%--------------------------------------------------------------------------
% paths and command to run C simulator.

filePath = '..\MHSL_SleepBand\src\';
sysPath  = '..\MHSL_SleepBand\Debug\';
ToBuild  = {'PLL', 'SW_detector', 'filters', 'main', 'sleep_detector2','HardConditions','iir','utils','VolumeControl'}; % All the names of functions needed to run the C simulator. Needed to compile 
sysRun   = 'MHSL_SleepBand.exe';    %Executable name

matPath = pwd;


%--------------------------------------------------------------------------
% Initialization
%--------------------------------------------------------------------------

%-- Load data path: indicate the path where the data to be analyzed is
load_path = '"D:\\SIM\\SimEEG\\HeaKids\\C_SIM\\C3\\"'; %'"C:\\Users\\fersterm\\Desktop\\"';
C_Parameters_Configuration(filePath, 'logger.h', 'LOAD_DATA_PATH', load_path)

%-- save data path: indicate teh path where results are saved
save_path = '"D:\\SIM\\SimOUT\\HeaKids\\C_mp"'; % '"C:\\Users\\fersterm\\Desktop\\"';
C_Parameters_Configuration(filePath, 'logger.h', 'DATA_SAVE_PATH', save_path)

%-- Case_ID: name of file/s to be tested
subject_v = {'WESAETH_EEG_D001_20180307_NAP_1'}; % Data load name. Add more than one to run several subjects 
test_name = '_mp'; % Used to identify specific runing test on the saved data

%-- case initialization
pll_opt = 0;        % Runs the pll optimization case
swd_opt = 0;        % Runs the swd optimization case
default_run = 1;    % Runs a default case

%-- By default enabling/disabling saving data 
C_Parameters_Configuration(filePath,'logger.h', 'DEBUGGING_ON', 1)  %debbuging_on = 1 always if not it will not save data.
    % SLEEP DETECTION
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_SLEEP_DETECTION', 0)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_FILTERED_DATA_SD', 0)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_SD_POW_D2', 0)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_SD_POW_D4', 0)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_SD_POW_B2', 0)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_SD_POW_MP', 0)

    % SW DETECTION
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_SW_DETECTION', 0)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_SW_TH', 0)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_SW_FILTERED_DATA', 0)

    %  PLL
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_PHASE_DETECTION', 0)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_PLL_ARGUMENT', 0)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_PLL_FILTERED_DATA', 0)
    
    %  PLL
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_STIM_CONDITION', 0)

    % HARD CONDITIONS
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_HARD_COND_1', 0)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_HARD_COND_2', 0)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_HARD_COND_3', 0)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_HARD_COND_4', 0)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_HARD_COND_5', 0)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_HC4_BETA_FILT_OUT', 0)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_HC4_BETA_AVG_POW', 0)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_HC5_ALPHA_FILT_OUT', 0)
C_Parameters_Configuration(filePath,'logger.h', 'SAVE_HC5_ALPHA_AVG_POW', 0) 


%--------------------------------------------------------------------------
% PLL parameter optimization
%--------------------------------------------------------------------------
if pll_opt==1        
    
    %-- PLL parameters variation
    pll_var_logg_name_v = {'TARGET_PHASE','GAIN_PD','GAIN_NCO'};
    pll_value_s.TARGET_PHASE = pi/4;
    pll_value_s.GAIN_PD      = 1:2:5;
    pll_value_s.GAIN_NCO     = 1:2:5;
    
    %-- Change config file according neccesary data log.
    C_Parameters_Configuration(filePath,'logger.h', 'SAVE_PHASE_DETECTION', 1)
    C_Parameters_Configuration(filePath,'logger.h', 'SAVE_STIM_CONDITION', 1)
    
    % PLL parameters
    var_name1 = pll_var_logg_name_v{1};
    var_name2 = pll_var_logg_name_v{2};
    var_name3 = pll_var_logg_name_v{3};
    
    
    for i1=1:length(pll_value_s.(var_name1))
        value_var1 = pll_value_s.(var_name1)(i1);
        C_Parameters_Configuration(filePath,'config.h', var_name1, value_var1)
        
        for i2=1:length(pll_value_s.(var_name2))
            value_var2 = pll_value_s.(var_name2)(i2);
            C_Parameters_Configuration(filePath,'config.h', var_name2, value_var2)
            
            for i3=1:length(pll_value_s.(var_name3))
                value_var3 = pll_value_s.(var_name3)(i3);
                C_Parameters_Configuration(filePath,'config.h', var_name3, value_var3)
                
                test_name = ['_PLL_',var_name1,'_',num2str(value_var1),'_',var_name2,'_',...
                    num2str(value_var2),'_',var_name3,'_',num2str(value_var3)];
                
                %-- Change logger file for subjects and run
                Run_Configuration_on_Subjects(subject_v, test_name, ToBuild, sysRun, filePath)
                
            end
        end
    end
    
end

%--------------------------------------------------------------------------
% SW detection parameter optimization
%--------------------------------------------------------------------------
if swd_opt==1
%-- SW detection filter


%-- SW detection parameters 


end

%--------------------------------------------------------------------------
% default run
%--------------------------------------------------------------------------

if default_run ==1
    %-- Change config file according neccesary data log.
    C_Parameters_Configuration(filePath,'logger.h', 'SAVE_SLEEP_DETECTION', 1)
    C_Parameters_Configuration(filePath,'logger.h', 'SAVE_SW_DETECTION', 1)
    C_Parameters_Configuration(filePath,'logger.h', 'SAVE_PHASE_DETECTION', 1)
    C_Parameters_Configuration(filePath,'logger.h', 'SAVE_STIM_CONDITION', 1)
    
    test_name = '_DEFAULT';
    Run_Configuration_on_Subjects(subject_v, test_name, ToBuild, sysRun, filePath)
    
end


%------------- END OF CODE --------------
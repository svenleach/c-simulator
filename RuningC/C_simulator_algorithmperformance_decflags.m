%% C_simulator_algorithmperformance_decflags.m: 
% Evaluates offline performance for C-simulator in combination with
% scoring,flags are in dec--> newer recordings from middle of May 2018 have
% that
% and specific settigns for one person

% Toolboxes required: 
% MAT-files required: none

% Authors with email: Caroline Lustenberger
% caroline.lustenberger@hest.ethz.ch
% Date of creation:23.05.2018
% Version: v1.0
% Validation date/version:
% Validated by: 
% Copyright: (c) 2018.  CAROLINE LUSTENBERGER, Mobile Health Systems Lab, ETH Zurich. All rights reserved
%------------- BEGIN CODE --------------

clear all
close all
clc


%% Define parameters

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
folder_path_c_output='F:\MLdataset_to_score\E014\SHAM_20180410\EEG\C_simulator\SW_Powermethod_noHC2\';

%IDUN folder
idun_folder='F:\MLdataset_to_score\E014\SHAM_20180410\EEG\';
data_file='WESAETH_EEG_E014_20180410QCffttot.mat';
visfile='E014_20180410_RB.vis';
epochLength=20;
fs=250;
thresh_beta=7.14;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Load provided files from C-simulator

cd(folder_path_c_output)
file_phase_detection=dir('*_PHASE_DETECTION.txt');
file_sleep_detection=dir('*_SLEEP_DETECTION.txt');
file_SW_detection=dir('*_SW_DETECTION.txt');  % If HC3 is implemented can be 2
file_stim_final=dir('*_STIM_CONDITION.txt'); % Has beeen set to 1 in config.h otherwise all will be 0 and resemble sham
file_HC1_final=dir('*_HARD_COND_1.txt'); % 
file_HC2_final=dir('*_HARD_COND_2.txt'); % 
file_HC3_final=dir('*_HARD_COND_3.txt'); % 
file_HC4_final=dir('*_HARD_COND_4.txt'); % 
%file_HC5_final=dir('*_HARD_COND_5.txt'); % 
file_SWF_final=dir('*_SW_FILTERED_DATA.txt'); % 
file_SWthresh_final=dir('*_SW_THRESHOLDS.txt'); % 
file_PLL_final=dir('*_PLL_FILTERED_INPUT_DATA.txt'); % 
file_SWfilt_final=dir('*_SW_FILTERED_DATA.txt'); % 
file_MP_final=dir('*_SD_POW_MP.txt'); %
file_delta2_final=dir('*_SD_POW_D2.txt'); %
file_delta4_final=dir('*_SD_POW_D4.txt'); %
file_beta2_final=dir('*_HC4_BETA_AVG_POW.txt'); %
%file_beta2_final=dir('*_SD_POW_B2.txt'); %
file_betafiltered_final=dir('*_HC4_BETA_FILT_OUT.txt'); %
file_volume=dir('*_VOLUME.txt'); %



C_phase_detectionID = fopen(file_phase_detection(1).name,'r'); 
C_phase_detection =  textscan(C_phase_detectionID,'%f','Delimiter',',');%;fscanf(C_phase_detectionID ,formatSpec);
C_phase_detection=C_phase_detection{1,1};
fclose(C_phase_detectionID);

C_sleep_detectionID = fopen(file_sleep_detection(1).name,'r'); 
C_sleep_detection =  textscan(C_sleep_detectionID,'%f','Delimiter',',');%;fscanf(C_phase_detectionID ,formatSpec);
C_sleep_detection=C_sleep_detection{1,1};
fclose(C_sleep_detectionID);

C_SW_detectionID = fopen(file_SW_detection(1).name,'r'); 
C_SW_detection =  textscan(C_SW_detectionID,'%f','Delimiter',',');%;fscanf(C_phase_detectionID ,formatSpec);
C_SW_detection=C_SW_detection{1,1};
fclose(C_SW_detectionID);

C_stim_detectionID = fopen(file_stim_final(1).name,'r'); 
C_stim_detection =  textscan(C_stim_detectionID,'%f','Delimiter',',');%;fscanf(C_phase_detectionID ,formatSpec);
C_stim_detection=C_stim_detection{1,1};
fclose(C_stim_detectionID);

C_HC1_detectionID = fopen(file_HC1_final(1).name,'r'); 
C_HC1_detection =  textscan(C_HC1_detectionID,'%f','Delimiter',',');%;fscanf(C_phase_detectionID ,formatSpec);
C_HC1_detection=C_HC1_detection{1,1};
fclose(C_HC1_detectionID);

C_HC2_detectionID = fopen(file_HC2_final(1).name,'r'); 
C_HC2_detection =  textscan(C_HC2_detectionID,'%f','Delimiter',',');%;fscanf(C_phase_detectionID ,formatSpec);
C_HC2_detection=C_HC2_detection{1,1};
fclose(C_HC2_detectionID);


C_HC3_detectionID = fopen(file_HC3_final(1).name,'r'); 
C_HC3_detection =  textscan(C_HC3_detectionID,'%f','Delimiter',',');%;fscanf(C_phase_detectionID ,formatSpec);
C_HC3_detection=C_HC3_detection{1,1};
fclose(C_HC3_detectionID);

C_HC4_detectionID = fopen(file_HC4_final(1).name,'r'); 
C_HC4_detection =  textscan(C_HC4_detectionID,'%f','Delimiter',',');%;fscanf(C_phase_detectionID ,formatSpec);
C_HC4_detection=C_HC4_detection{1,1};
fclose(C_HC4_detectionID);

% C_HC5_detectionID = fopen(file_HC5_final(1).name,'r'); 
% C_HC5_detection =  textscan(C_HC5_detectionID,'%f','Delimiter',',');%;fscanf(C_phase_detectionID ,formatSpec);
% C_HC5_detection=C_HC5_detection{1,1};
% fclose(C_HC5_detectionID);


C_SWF_detectionID = fopen(file_SWF_final(1).name,'r'); 
C_SWF_detection =  textscan(C_SWF_detectionID,'%f','Delimiter',',');%;fscanf(C_phase_detectionID ,formatSpec);
C_SWF_detection=C_SWF_detection{1,1};
fclose(C_SWF_detectionID);

C_SWthresh_detectionID = fopen(file_SWthresh_final(1).name,'r'); 
C_SWthresh_detection =  textscan(C_SWthresh_detectionID,'%f','Delimiter',',');%;fscanf(C_phase_detectionID ,formatSpec);
C_SWthresh_detection=C_SWthresh_detection{1,1};
fclose(C_SWthresh_detectionID);

C_PLL_detectionID = fopen(file_PLL_final(1).name,'r'); 
C_PLL_detection =  textscan(C_PLL_detectionID,'%f','Delimiter',',');%;fscanf(C_phase_detectionID ,formatSpec);
C_PLL_detection=C_PLL_detection{1,1};
fclose(C_PLL_detectionID);

C_SWfilt_detectionID = fopen(file_SWfilt_final(1).name,'r'); 
C_SWfilt_detection =  textscan(C_SWfilt_detectionID,'%f','Delimiter',',');%;fscanf(C_phase_detectionID ,formatSpec);
C_SWfilt_detection=C_SWfilt_detection{1,1};
fclose(C_SWfilt_detectionID);

C_delta2_detectionID = fopen(file_delta2_final(1).name,'r'); 
C_delta2_detection =  textscan(C_delta2_detectionID,'%f','Delimiter',',');%;fscanf(C_phase_detectionID ,formatSpec);
C_delta2_detection=C_delta2_detection{1,1};
fclose(C_delta2_detectionID);

C_beta2_detectionID = fopen(file_beta2_final(1).name,'r'); 
C_beta2_detection =  textscan(C_beta2_detectionID,'%f','Delimiter',',');%;fscanf(C_phase_detectionID ,formatSpec);
C_beta2_detection=C_beta2_detection{1,1};
fclose(C_beta2_detectionID);

C_delta4_detectionID = fopen(file_delta4_final(1).name,'r'); 
C_delta4_detection =  textscan(C_delta4_detectionID,'%f','Delimiter',',');%;fscanf(C_phase_detectionID ,formatSpec);
C_delta4_detection=C_delta4_detection{1,1};
fclose(C_delta4_detectionID);

C_MP_detectionID = fopen(file_MP_final(1).name,'r'); 
C_MP_detection =  textscan(C_MP_detectionID,'%f','Delimiter',',');%;fscanf(C_phase_detectionID ,formatSpec);
C_MP_detection=C_MP_detection{1,1};
fclose(C_MP_detectionID);

C_Volume_ID = fopen(file_volume(1).name,'r'); 
C_Volume =  textscan(C_Volume_ID,'%f','Delimiter',',');%;fscanf(C_phase_detectionID ,formatSpec);
C_Volume=C_Volume{1,1};
fclose(C_Volume_ID);


% C_beta2filt_detectionID = fopen(file_betafiltered_final(1).name,'r'); 
% C_beta2filt_detection =  textscan(C_beta2filt_detectionID,'%f','Delimiter',',');%;fscanf(C_phase_detectionID ,formatSpec);
% C_beta2filt_detection=C_beta2filt_detection{1,1};
% fclose(C_beta2filt_detectionID);

%% Load visfile and data file
cd(idun_folder)
load(data_file)

[vistrack,vissymb,offs] = readtrac([idun_folder,visfile],1);
visgood=find(sum(vistrack')==0);

%% NREM performance

% Reshape sleepstaging
sleepStage = [];
    for i=1:length(vissymb)
        if ~isempty(str2num(vissymb(i)))
            sleepStage(i) = str2num(vissymb(i));
            if str2num(vissymb(i))==4
                 sleepStage(i) = 3;
            end
        else
            sleepStage(i) = 4; %4=rem
        end
    end
    
     % Upsampling of sleep stage to match with 4s power windows
    usampSleepStage = reshape(repmat(sleepStage,epochLength*fs,1),[],1);
    usampSleepStage_NREM = (usampSleepStage==2 | usampSleepStage==3); %Define only NREM sleep vs no-NREM sleep   
    usampSleepStage_NREM1 = (usampSleepStage==1 | usampSleepStage==2 | usampSleepStage==3); %Define only NREM sleep vs no-NREM sleep   


    for n=1:size(usampSleepStage_NREM,2)
       cp(1,n)=length(find(usampSleepStage_NREM+C_sleep_detection(1:length(usampSleepStage_NREM),n)==2)); %correct positive
       fp(1,n)=length(find(usampSleepStage_NREM-C_sleep_detection(1:length(usampSleepStage_NREM),n)==-1));% false positive
       fn(1,n)=length(find(usampSleepStage_NREM-C_sleep_detection(1:length(usampSleepStage_NREM),n)==1)); %false negative
       tn(1,n)=length(find(usampSleepStage_NREM+C_sleep_detection(1:length(usampSleepStage_NREM),n)==0)); %true negative
    end
    
    NREMrecall = cp./(cp+fn); %ratio of detected positives to total positives, also called recall
    NREMprec = cp./(cp+fp); %ratio of detections that are correct, called precision
    Fscore=((NREMrecall.*  NREMprec)./(NREMrecall +  NREMprec)).*2;
    
    
    
%% Slow wave detection performance
    % Stim via 3 main lines
    SWdetected_algo_index_all=C_SW_detection+C_phase_detection+C_sleep_detection;
    SWdetected_algo_index=SWdetected_algo_index_all==3;
    
    % Stim via stim trigger (real stim)
     length(find(C_stim_detection))


    SW_latency_indx=find(C_stim_detection(1:length(usampSleepStage))==1); % real stim
    %SW_thresh_indx=find(C_SWthresh_detection(1:length(usampSleepStage))==1);
    SW_detect_indx=find(C_SW_detection(1:length(usampSleepStage))==1);
    
    SW_selection_realscore=usampSleepStage(SW_latency_indx);
    SWinN1=length(find(SW_selection_realscore==1));
    SWinW=length(find(SW_selection_realscore==0));
    SWinN2=length(find(SW_selection_realscore==2));
    SWinN3=length(find(SW_selection_realscore==3));
    SWinR=length(find(SW_selection_realscore==4));
    SWall=length(SW_latency_indx);


    ErrorW_R=((SWinR+SWinW)./SWall).*100;
    ErrorW_R_N1=((SWinR+SWinW+SWinN1)./SWall).*100;
    CorrectNREMwN1=((SWinN1+SWinN2+SWinN3)./SWall).*100;
    CorrectNREM=((SWinN2+SWinN3)./SWall).*100;

    
      % Generate table

     Trial_numbers=[SWinW; SWinN1;SWinN2;SWinN3;SWinR;SWall;ErrorW_R;ErrorW_R_N1;CorrectNREMwN1;CorrectNREM;NREMrecall; NREMprec; Fscore];
     Rownames={'SWinW', 'SWinN1','SWinN2','SWinN3','SWinR','SWall','ErrorW_R','ErrorW_R_N1','CorrectNREMwN1','CorrectNREM','NREMrecall','NREMprec','Fscore'};
     
     T_trial=table(Trial_numbers,'RowNames',Rownames);
     
     
     f=figure();
     
     TString=evalc('disp(T_trial)');
     uitable(f,'Data',table2cell(T_trial),'ColumnName','Trial numbers','RowName',Rownames)
     snapnow
     close all


%% PLL performance
% Check precision of phase
   % Notchfilter the data
    wo = 50/(fs/2);  bw = wo/15;
    [num,den] = iirnotch(wo,bw);
    notch_filt_data = filter(num,den,data(:,2));
    notch_filt_data_filtfilt = filtfilt(num,den,data(:,2));
   
%     % Highpass butter data
%     high_cutoff=0.1;
%     order=1;
%     [b14, a14]=butter(order,(high_cutoff/(fs/2)),'high');
%     notch_filt_data_filtfilt=filtfilt(b14,a14,notch_filt_data_filtfilt);

 %filter data in the delta range
 
  % Basic 2nd order bandpass butter for general EEG
    EEGFiltLow = 0.5;
   EEGFiltHi = 40;
    filtOrder=2;

    [NEEG, DEEG] = butter(filtOrder,[EEGFiltLow, EEGFiltHi]./(fs/2),'bandpass');
    EEGfilt_butter = filtfilt(NEEG,DEEG,notch_filt_data_filtfilt);
    [z,p,k] = butter(filtOrder,[EEGFiltLow, EEGFiltHi]./(fs/2),'bandpass');
     sos = zp2sos(z,p,k);
    fvtool(sos,'Analysis','freq','Fs',fs)
    
    % Basic 2nd order bandpass butter
    deltaFiltLow = 0.5;
    deltaFiltHi = 4;
    filtOrder=2;

    [Ndelta, Ddelta] = butter(filtOrder,[deltaFiltLow, deltaFiltHi]./(fs/2),'bandpass');
    EEG_deltafilt_butter2 = filtfilt(Ndelta,Ddelta,notch_filt_data_filtfilt);
    EEG_deltafilt_butter2_single = filter(Ndelta,Ddelta,notch_filt_data);
    figure();plot(EEG_deltafilt_butter2);hold on;plot(EEG_deltafilt_butter2_single,'r')
    
    [z,p,k] = butter(filtOrder,[deltaFiltLow, deltaFiltHi]./(fs/2),'bandpass');
    sos = zp2sos(z,p,k);
    fvtool(sos,'Analysis','freq','Fs',fs)
    snapnow
    % MHSL filter
       N1=2;   
        bpFilt = designfilt('bandpassiir','FilterOrder',2*N1, ...
         'HalfPowerFrequency1',0.2,'HalfPowerFrequency2',5, ...
         'SampleRate',fs);
        fvtool(bpFilt,'analysis','freq');
        snapnow
        DataFiltered_MHSL = filtfilt(bpFilt,notch_filt_data_filtfilt);
        DataFiltered_MHSL_single = filter(bpFilt,notch_filt_data);
        
        figure();plot(DataFiltered_MHSL);hold on;plot(DataFiltered_MHSL_single,'r')
        clear bpFilt
    % KISPI filter
    
         N1=3;        
        
         bpFilt = designfilt('bandpassiir','FilterOrder',2*N1, ...
         'HalfPowerFrequency1',0.319825,'HalfPowerFrequency2',3.12648, ...
         'SampleRate',fs);
        snapnow
     DataFiltered_KISPI = filtfilt(bpFilt,notch_filt_data_filtfilt);
     DataFiltered_KISPI_single = filter(bpFilt,notch_filt_data);
     fvtool(bpFilt,'analysis','freq');
     clear bpFilt
    % Chebby bandpass '05_to_4Hz' 
        Rp  = 0.05; % Ripple in dB
        Rs  = 60;   % Attenuation in the bandstop in dB
        [fs1, fc1, fc2, fs2] = deal(0.1, 0.5, 3.95, 6); % Type1
        
        [FOrder,WpC1] = cheb1ord([fc1 fc2]/(fs/2),[fs1 fs2]/(fs/2),Rp,Rs);
        
         bpFilt = designfilt('bandpassiir','FilterOrder',FOrder*2, ...
        'PassbandFrequency1',fc1,'PassbandFrequency2',fc2, ...
        'SampleRate',fs,'DesignMethod','cheby1','PassbandRipple',Rp);   
        fvtool(bpFilt,'analysis','freq');
        snapnow
        
        DataFiltered_Chebby = filtfilt(bpFilt,notch_filt_data_filtfilt);
        clear bpFilt
        
        
         % Chebby bandpass '05_to_2Hz' 
        Rp  = 0.05; % Ripple in dB
        Rs  = 60;   % Attenuation in the bandstop in dB
        [fs1, fc1, fc2, fs2] = deal(0.1, 0.5, 2, 4); % Type1
        
        [FOrder,WpC1] = cheb1ord([fc1 fc2]/(fs/2),[fs1 fs2]/(fs/2),Rp,Rs);
        
         bpFilt = designfilt('bandpassiir','FilterOrder',FOrder*2, ...
        'PassbandFrequency1',fc1,'PassbandFrequency2',fc2, ...
        'SampleRate',fs,'DesignMethod','cheby1','PassbandRipple',Rp);   
        fvtool(bpFilt,'analysis','freq');
        snapnow
        
        DataFiltered_Chebby2 = filtfilt(bpFilt,notch_filt_data_filtfilt);
        clear bpFilt
        
%         %% EEGLAB FIR filter 0.7-1Hz Ong et al. similar
%         Data_filtered_FIR_07_1=eegfilt(notch_filt_data',fs,0.7,1);
%         
  % Calculate Hilbert      
    output_butter2=hilbert(EEG_deltafilt_butter2); 
    SW_flag_hilbert_radian_angle_butter2=angle(output_butter2(SW_latency_indx)*exp(1i*pi/2)); % in radians, Hilber introduces a shift of pi/2--> has to be corrected
    
    output_MHSL=hilbert(DataFiltered_MHSL); 
    SW_flag_hilbert_radian_angle_MHSL=angle(output_MHSL(SW_latency_indx)*exp(1i*pi/2)); % in radians, Hilber introduces a shift of pi/2--> has to be corrected
  
      output_KISPI=hilbert(DataFiltered_KISPI); 
    SW_flag_hilbert_radian_angle_KISPI=angle(output_KISPI(SW_latency_indx)*exp(1i*pi/2)); % in radians, Hilber introduces a shift of pi/2--> has to be corrected
    
     output_Chebby=hilbert(DataFiltered_Chebby); 
    SW_flag_hilbert_radian_angle_Chebby=angle(output_Chebby(SW_latency_indx)*exp(1i*pi/2)); % in radians, Hilber introduces a shift of pi/2--> has to be corrected
 
         output_Chebby2=hilbert(DataFiltered_Chebby2); 
    SW_flag_hilbert_radian_angle_Chebby2=angle(output_Chebby2(SW_latency_indx)*exp(1i*pi/2)); % in radians, Hilber introduces a shift of pi/2--> has to be corrected

%     output_FIR07_1=hilbert(Data_filtered_FIR_07_1); 
%     SW_flag_hilbert_radian_angle_FIR_07_1=angle(output_FIR07_1(SW_latency_indx)*exp(1i*pi/2)); % in radians, Hilber introduces a shift of pi/2--> has to be corrected

    % plot polar histogram
    close all
    figure
   % subplot(2,3,1)
    polarhistogram(SW_flag_hilbert_radian_angle_butter2,25);
    title('butter')
     subplot(2,3,2)
    polarhistogram(SW_flag_hilbert_radian_angle_MHSL,25);
    title('MHSL')
     subplot(2,3,3)
    polarhistogram(SW_flag_hilbert_radian_angle_KISPI,25);
    title('KISPI')
     subplot(2,3,4)
    polarhistogram(SW_flag_hilbert_radian_angle_Chebby,25);
    title('Chebby')
    subplot(2,3,5)
    polarhistogram(SW_flag_hilbert_radian_angle_Chebby2,25);
    title('Chebby2')
%      subplot(2,3,5)
%     polarhistogram(SW_flag_hilbert_radian_angle_FIR_07_1,25);
%     title('FIR07_1')
%     snapnow

    
    %% Upsample delta 2 delta 4
    
        upsampDelta2 = reshape(repmat(C_delta2_detection',4*fs,1),[],1);
     upsampDelta4 = reshape(repmat(C_delta4_detection',4*fs,1),[],1);
       
    %% Plot multiple data
    % Flag plots relative to delta activity
    timeindex=1:250:length(DataFiltered_KISPI);
    figure()
    ax(1) = subplot(4,1,1);
    plot(DataFiltered_KISPI_single,'b');
    hold on
    plot(DataFiltered_KISPI,'k');
    hold on
     plot(C_SWfilt_detection,'r--');
    hold on
    plot(SW_latency_indx,C_SWfilt_detection(SW_latency_indx),'*r')
    hold on
    plot(SW_detect_indx,C_SWfilt_detection(SW_detect_indx),'*g')
    hold on
    %plot(SW_thresh_indx,C_SWfilt_detection(SW_thresh_indx),'*m')
 %xticks(timeindex)
 %grid on
    hline(-40)
    hline(0)
    yaxis([-100 100])

     ax(2) = subplot(4,1,2);
   
    plot(abs(C_SWfilt_detection).^2,'k');
 yaxis([0 10000])
    linkaxes(ax,'x');
    snapnow
    
       ax(3) = subplot(4,1,3);
    plot(EEGfilt_butter);
    hold on
    plot(SW_latency_indx,EEGfilt_butter(SW_latency_indx),'*r')
    yaxis([-150 150])
    linkaxes(ax,'x');
    snapnow
      
    
 % Flag plots relative to delta activity
    timeindex=1:250:length(C_SWfilt_detection);
    figure()
    ax(1) = subplot(2,1,1);
    plot(C_SWfilt_detection);
    hold on
    plot(SW_latency_indx,C_SWfilt_detection(SW_latency_indx),'*r')
    hold on
    plot(SW_detect_indx,C_SWfilt_detection(SW_detect_indx),'*g')
    %xticks(timeindex)
    %grid on
    hline(-40)
    yaxis([0 100])
    ax(2) = subplot(2,1,2);
    plot(C_PLL_detection);
    hold on
    plot(SW_latency_indx,C_PLL_detection(SW_latency_indx),'*r')
%     yaxis([0 100])
    linkaxes(ax,'x');
    snapnow
  
 
    % Plot thresholds NREM detetcion
      TestName='Test'
    [SD_flag_M,SD_flag_MwFC] = NREM_SD_Matlab(notch_filt_data,TestName,0,'e'); % sleep detection flags from Matlab
    snapnow

    %% Create delta filter to define good SW detection strategy
        %% filter data to look at data
        
        rawFz=data(:,2);
    % generate filter settings
    mvaLength = fs*4; %times length of window
    filtOrder = 2; %since bandpassing, actual filter order is filterOrder*2
    %spindle detection filter settings (in Hz) Fz-Cz datastream
   
    deltaFiltLow = .5;
    deltaFiltHi = 4;
      %generate transfer function filter coefficients

   [Ndelta Ddelta] = butter(filtOrder,[deltaFiltLow, deltaFiltHi]./(fs/2),'bandpass');
     
    %filter rawFz using filters defined above
    deltaFiltFz = filter(Ndelta,Ddelta,rawFz);
     %calcualte 1s static averages of filtered signal
    avgDelta = [];
     %shifts each value forward in time by one position to reflect real-time
    %calculation of mean and rms
    for iMean = 1:floor(length(deltaFiltFz))-1
        avgDelta_rms(iMean+1) = (mean(deltaFiltFz((iMean-1)+1:iMean).^2));
    
    end
    %First sample period is set to 0 to reflect initialization
    mvaAvgsDelta_rms = filter(ones(1,mvaLength)./mvaLength,1,avgDelta_rms);
   
    upsampDelta_power_rms = mvaAvgsDelta_rms ;
    %upsampDelta_power_rms = reshape(repmat(mvaAvgsDelta,2*fs,1),[],1);
% 
% figure();plot(upsampDelta_power_rms)
% yaxis([0 10000])
% %plot
%     timeindex=0:1/250:length(C_beta2_detection)/250;   
%     figure()
%     ax(1) = subplot(4,1,1);
%     plot(timeindex(1:length(upsampDelta_power_rms)),DataFiltered_KISPI_single(1:length(upsampDelta_power_rms)),'b');
%     hold on
%     plot(timeindex(1:length(upsampDelta_power_rms)),DataFiltered_KISPI(1:length(upsampDelta_power_rms)),'k');
%     hold on
%     plot(timeindex(1:length(upsampDelta_power_rms)),C_SWfilt_detection(1:length(upsampDelta_power_rms)),'r--');
%     hold on
%     plot(timeindex(SW_latency_indx),C_SWfilt_detection(SW_latency_indx),'*r')
%     hold on
%     plot(timeindex(SW_detect_indx),C_SWfilt_detection(SW_detect_indx),'*g')
%     hold on
%     %plot(SW_thresh_indx,C_SWfilt_detection(SW_thresh_indx),'*m')
% 
%     yaxis([-100 100])
% 
%      ax(2) = subplot(4,1,2);
% 
%     plot(timeindex(1:length(upsampDelta_power_rms)),abs(EEG_deltafilt_butter2(1:length(upsampDelta_power_rms))).^2,'k');
%     hold on
%      plot(timeindex(SW_latency_indx),EEG_deltafilt_butter2(SW_latency_indx).^2,'*r')
%  yaxis([0 15000])
%     %linkaxes(ax,'x');
%  
%     
%        ax(3) = subplot(4,1,3);
%     plot(timeindex(1:length(upsampDelta_power_rms)),EEGfilt_butter(1:length(upsampDelta_power_rms)));
%     hold on
%     plot(timeindex(SW_latency_indx),EEGfilt_butter(SW_latency_indx),'*r')
%     hold on
%     
%     yaxis([-150 150])
% 
%     
%      ax(4) = subplot(4,1,4)
%      
%         figure()
%     plot(timeindex(1:length(upsampDelta_power_rms)),upsampDelta_power_rms);
%     hold on
%     plot(timeindex(SW_latency_indx),upsampDelta_power_rms(SW_latency_indx),'*r')
%     hold on
%    
%     % plot(timeindex(1:length(upsampDelta_power_rms)),C_SWfilt_detection(1:length(upsampDelta_power_rms)),'c')
%   
%     yaxis([0 5000])  
%   hline(200)
%         % linkaxes(ax,'x');
%          
%          
%     
%     figure()
%           
%   %  plot(C_HC4_detection.*thresh_beta);
% %     hold on
%  plot(timeindex(1:end-1),C_beta2_detection,'r');
% %     hold on
% %     plot(SW_latency_indx,C_HC4_detection(SW_latency_indx),'*g')
%     hold on
%  plot(timeindex(1:end-1),C_HC1_detection.*4,'c')
%      hold on
%      plot(timeindex(1:end-1),C_sleep_detection.*3,'m')
%     yaxis([0 10])  
%     hline(thresh_beta)
%     
%     
% %% Mark wrong stims
% 
%     SWinN1_indx= SW_latency_indx(find(SW_selection_realscore==1));
%     SWinW_indx=SW_latency_indx(find(SW_selection_realscore==0));
%     SWinR_indx=SW_latency_indx(find(SW_selection_realscore==4));
% 
%     sleep_detection_realdata=(data(:,11)==1 | data(:,11)==3 |data(:,11)==5 | data(:,11)==7);
%     
%     figure()
%     
%      %ax(1) = subplot(3,1,1);
%         %     hold on
%      plot(C_beta2_detection,'y');
% %     hold on
% %     plot(SW_latency_indx,C_HC4_detection(SW_latency_indx),'*g')
%      hold on
%      plot((data(:,11)==7).*8,'r*')
%       hold on
%      plot(sleep_detection_realdata.*8,'m')
%       hold on
%      plot((C_stim_detection==1).*10,'r*')
%      hold on
%      plot(C_sleep_detection.*2,'b')
%      hold on
% %     plot(C_HC1_detection.*3,'c')  
% %      hold on
% %       plot(C_HC2_detection.*4,'b')
% %       hold on
% %       plot(C_HC3_detection.*5,'g')
% %       hold on
% % %       plot(C_HC4_detection.*6,'k')
% % %       hold on
% % %      plot(C_HC5_detection.*6,'r')
% %      
%    yaxis([0 20])  
% 
%     
%        ax(2) = subplot(3,1,2);
%     plot(EEGfilt_butter(1:length(upsampDelta_power_rms)));
%     hold on
%     plot(SW_latency_indx,EEGfilt_butter(SW_latency_indx),'*r')
%     yaxis([-150 150])
% 
%     
%      ax(3) = subplot(3,1,3);
%     plot(upsampDelta_power_rms);
%     hold on
%     plot(SWinN1_indx,upsampDelta_power_rms(SWinN1_indx),'*g')
%      hold on
%     plot(SWinR_indx,upsampDelta_power_rms(SWinR_indx),'*m')
%      hold on
%     plot(SWinW_indx,upsampDelta_power_rms(SWinW_indx),'*r')
%     hold on
%     plot(C_HC1_detection.*400,'c')
%      hold on
%      plot(C_sleep_detection.*300,'m')
%        hold on
%      plot(C_HC4_detection.*500,'k')
%          hold on
% %     plot(C_HC5_detection.*600,'r')
%      yaxis([0 5000])  
%       hline(200)
%          linkaxes(ax,'x');
%          
%          %% Compare C and real data detection
%          
%            figure()
%     
%     
%         %     hold on
%      plot(EEG_deltafilt_butter2.^2,'y');
%     hold on
%     plot(data(:,11).*100,'b')
%      hold on
%      plot(C_sleep_detection.*1000,'r')
%        hold on
%   
%     yaxis([0 5000])  
    
    %% Plot Gordon
      timeindex=0:1/250:length(C_beta2_detection)/250;   
      usampSleepStage_NREM_NaN=double(usampSleepStage_NREM);
      usampSleepStage_NREM_NaN(usampSleepStage_NREM==0)=NaN;
   figure
    plot(timeindex(1:length(usampSleepStage_NREM_NaN)),abs(EEG_deltafilt_butter2(1:length(usampSleepStage_NREM_NaN))).^2,'b');
    hold on
     plot(timeindex(SW_latency_indx),EEG_deltafilt_butter2(SW_latency_indx).^2,'*r')
     hold on
      plot(timeindex(1:length(usampSleepStage_NREM_NaN)),usampSleepStage_NREM_NaN(1:length(usampSleepStage_NREM_NaN)).*14000,'ok')
    yaxis([0 15000])
        
    
   figure
    plot(timeindex(1:length(usampSleepStage_NREM_NaN)),(EEGfilt_butter(1:length(usampSleepStage_NREM_NaN))),'k');
    hold on
     plot(timeindex(SW_latency_indx),EEGfilt_butter(SW_latency_indx),'*r')
    yaxis([-100 100])
    
    %% Plot volume
    timeindex=0:1/250:length(C_beta2_detection)/250;   
      usampSleepStage_NREM_NaN=double(usampSleepStage_NREM);
      usampSleepStage_NREM_NaN(usampSleepStage_NREM==0)=NaN;
   figure
    plot(timeindex(1:length(usampSleepStage_NREM_NaN)),abs(EEG_deltafilt_butter2(1:length(usampSleepStage_NREM_NaN))).^2,'c');
    hold on
     plot(timeindex(1:length(usampSleepStage_NREM_NaN)),C_Volume(1:length(usampSleepStage_NREM_NaN)).*100,'r')
      yaxis([0 15000])
        
   
      
      %% HC check
      
          
    figure()
       plot(C_beta2_detection,'y');
    hold on
    plot(SW_latency_indx,C_HC4_detection(SW_latency_indx),'g')
      hold on
     plot((C_stim_detection==1).*10,'r*')
           hold on
     plot((C_sleep_detection==1).*15,'m*')
     hold on
     plot((C_phase_detection==1).*16,'g*')
            hold on
     plot((C_SW_detection==1).*18,'b*')
     hold on
     plot(C_sleep_detection.*2,'b')
     hold on
    plot(C_HC1_detection.*3,'c')  
     hold on
      plot(C_HC2_detection.*4,'b')
      hold on
      plot(C_HC3_detection.*5,'g')
      hold on
      plot(C_HC4_detection.*6,'k')
    yaxis([0 20])  
    
    
    %% Define ISI

Diff_latency=(diff(SW_latency_indx))./250;
Diff_latency_tooshort=find(Diff_latency<0.5);


binranges=[0,0.5,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
bincounts=histc(Diff_latency,binranges);

figure()
bar(binranges,bincounts,'histc')
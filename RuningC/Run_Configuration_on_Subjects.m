% function Run_Configuration_on_Subjects(subject_v, test_name, ToBuild, sysRun, filePath)
% Run_Configuration_on_Subjects - after the config file was set with the
% desired configuration, this function runs the script for different
% subjects
%
% Syntax:  Run_Configuration_on_Subjects(subject_v, test_name, ToBuild, sysRun, filePath)
%
% Inputs:
%    subject_v - contains the list name of subject to run the simulation
%    test_name - name of the test. Added to subject name on data saved file
%                name
%    ToBuild   - name of C files to be built 
%    sysRun    - command to run the C simulator
%    filePath  - path of the configuration/logger file
%
% Authors with email: Maria Laura Ferster
% (maria.ferster@hest.ethz.ch)
% Date of creation: 30.10.2017
% Version: v0.1
% Validation date/version: 30.10.2017
% Validated by: Laura Ferster
% Copyright: (c) 2017.  MARIA LAURA FERSTER, Mobile Health Systems Lab, ETH Zurich. All rights reserved

%------------- BEGIN CODE --------------

function Run_Configuration_on_Subjects(subject_v, test_name, ToBuild, sysRun, filePath)

    for i=1:length(subject_v)

        subject = subject_v{i};
        var_logg_name_v = {'DATA_LOAD_NAME','DATA_SAVE_NAME'};
        value_s.DATA_LOAD_NAME = ['"',subject,'"'];
        value_s.DATA_SAVE_NAME = ['"',subject,test_name,'"'];

        % change config/logger file
        for k=1:length(var_logg_name_v)
            C_Parameters_Configuration(filePath, 'logger.h', var_logg_name_v{k}, value_s.(var_logg_name_v{k}))
        end
        % build

        cd ..\MHSL_SleepBand\Debug\
        sysBuild = 'gcc  -o "MHSL_SleepBand.exe" ';
        for n=1:length(ToBuild)
            sysToBuild = ['gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"src/',ToBuild{n},'.d" -MT"src/',ToBuild{n},'.o" -o "src/',ToBuild{n},'.o" "../src/',ToBuild{n},'.c"'];
            system(sysToBuild);
            sysBuild = [sysBuild, ' ./src/',ToBuild{n},'.o '];
        end
        system(sysBuild);

        %run
        system(sysRun);

        %back to original path
        cd ../../RuningC/

    end

end

%------------- END OF CODE --------------